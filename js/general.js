$(document).ready(function(){

	var general_app = {


		ready: function(){

			var t = general_app.components;

			t.data_picker();
			t.data_time_picker();
			t.prevent_double_click_on_submit_form();
			t.table_row_to_card();

			// $(window).resize(function(){
			// 	t.table_row_to_card();
			// });

		}, 

		components: {

			data_picker: function(){
				var defaultDate = moment().toDate();
				
				$( ".datepicker" ).datetimepicker({
                    format: 'DD-MM-YYYY',
                    defaultDate: defaultDate
                });
			},

			data_time_picker: function(){
				var defaultDate = moment().toDate();
				
				$( ".time" ).datetimepicker({
                    format: 'H:m:s',
                    defaultDate: defaultDate
                });
			},

			prevent_double_click_on_submit_form: function(){

				$('form').on('submit', function(){


					var form = $(this);

					var button = $('button', form);

					var external_button = $('button[form="'+ form.attr('id') +'"]');

					button.attr('disabled', 'disabled');

					external_button.attr('disabled', 'disabled');


				});

			},

			table_row_to_card: function(){

				if($(window).width() < 768){
					
					var tables = $('.table-responsive');

					tables.find('table').each(function(){

						var table = $(this);

						var get_thead = $('thead', table);

						var get_body = $('tbody', table);

						var th_titles = [];

						var tbody_items = [];

						var cards = [];

						var get_thead_titles = (function(){

							var th_list = $('th', get_thead);

							th_list.each(function(){

								var th = $(this);

								var th_value = th.text();

								th_titles.push(th_value);

							});

						}());

						var create_card_from_each_item = (function(){

							var tr_list = $('tr', get_body);

							tr_list.each(function(){

								var tr = $(this);

								var tr_td = $('td', tr);

								var tr_td_values = [];

								tr_td.each(function(i, e){

									var td = $(this);

									tr_td_values.push(td.html());

								});

								cards.push(tr_td_values);

							});

						}());

						table.hide();

						var parse_cards = (function(){

							var parent = table.parent();

							$.each(cards, function(i, card){

								var card_data = function(){

									var data = '';

									$.each(card, function(i, o){

										if(th_titles[i] != undefined){

											data += `
												<p class="m-0"><b>`+th_titles[i]+`</b></p>
												<p>`+o+`</p>
											`;
											
										}else{

											data += `<p>`+o+`</p>`;

										}


									});

									return data;

								}

								var card_html = `
									
									<div class="panel-body panel-mobile-table" style="border-bottom: 1px solid #ddd; padding-bottom:0">
										`
										+card_data()+ 

										`
									</div>

								`;

								parent.append(card_html);

							});

						}());

					});

				}else{

					$('.panel-mobile-table').hide();

					$('.table-responsive table').show();

				}

			}

		}


	}

	general_app.ready();

});