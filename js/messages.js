var msg = {


	ready: function(){

		var t = msg.methods;

		t.on_click_image_show_modal();

	}, 


	methods: {


		on_click_image_show_modal: function(){


			$('.thumb_img').click(function(){

				var thumb = $(this);

				var img = $('<img />', {
					src: thumb.data('thumb'),
					class: 'img-fluid'
				});

				$('.display_thumb_img').html('').append(img);

			});


		}


	}


}

msg.ready();
