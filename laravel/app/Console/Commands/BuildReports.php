<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class BuildReports extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'build-reports';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reportes del día';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
    }
}
