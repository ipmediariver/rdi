<?php

namespace App\Console\Commands;

use App\Models\Users\User;
use Illuminate\Console\Command;
use App\Notifications\MontlyIncidentsReport;
use Notification;

class MonthlyIncidentsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'monthly-report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create reporting of the last month from zones RDI';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $contacts = User::where('role','contact')->get();

        if ( $contacts->count() > 0 ) {
            foreach ($contacts as $contact) {

                Notification::send( $contact, new MontlyIncidentsReport( $contact->zones ) );

            }
        }
    }
}
