<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Users\User;
use App\Models\Zones\Zone;
use App\Models\Zones\Message;
use App\Notifications\ReportingMessages;
use App\Notifications\ReportingMessagesContacts;
use App\Notifications\MessagesNonVerified;
use Notification;

class SendMailReportingMessages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'daily-report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create reporting of recent messages from zones RDI';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $zones = Zone::get();

        $admins = User::where('role','admin')->get();
        
        if ( !empty( $this->validate_send_mail( $zones ) ) ) {

            Notification::send( $admins, new ReportingMessages( $zones ) );
        }

        if ( !empty( $this->validate_send_mail_non_verified( $zones ) ) ) {
            
            Notification::send( $admins, new MessagesNonVerified( $zones ) );
        }

        $users = User::where('role','contact')->get();
        
        foreach ($users as $user) {
            $zones = $user->contact ? $user->contact->zones() : null;
            if ($zones) {

                if ( !empty( $this->validate_send_mail( $zones ) ) ) {

                    Notification::send( $user, new ReportingMessagesContacts( $zones ) );

                }
                
            }
        }

        $this->info('El reporte diario ha sido creado y enviado satisfactoriamente.');
    }

    public function validate_send_mail( $zones, $to_contacts = false )
    {
        $result = [];

        foreach ($zones as $zone) {

            if ($to_contacts) {

                $messages = $zone->messages()->where( 'verified', 1 )->whereDate( 'created_at', date('Y-m-d') );

            } else {

                $messages = $zone->messages()->whereDate( 'created_at', date( 'Y-m-d' ) );

            }

            if ($messages->count() > 0) {

                $result[] = 1;

            }                
        }
        
        return $result;
    }

    public function validate_send_mail_non_verified( $zones )
    {
        $result = [];

        foreach ($zones as $zone) {

            $messages = $zone->messages()
                    ->where('verified', 0)
                    ->whereDate('created_at','>=', date('Y-m-d',strtotime('-1 week')) )
                    ->whereDate('created_at','<=', date('Y-m-d') );

            if ( $messages->count() > 0 ) {

                $result[] = 1;

            }                
        }
        
        return $result;
    }
}
