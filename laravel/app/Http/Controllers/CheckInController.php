<?php

namespace App\Http\Controllers;

use App\Models\Users\CheckIn;
use App\Models\Zones\Zone;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CheckInController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( $zone )
    {
        $zone = Zone::where( 'zone_id', $zone )->first();

        $checks = $zone->checkIns()->where('zone_id', $zone->zone_id )->latest()->paginate(20);

        return view('users.check-in.index', compact('checks', 'zone'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $zone_id)
    {
        $checkIn = new CheckIn();

        $checkIn->zone_id  = $zone_id;
        $checkIn->agent_id = auth()->user()->agent->agent_id;
        $checkIn->check_in = Carbon::now()->format('Y-m-d H:i:s');

        $checkIn->save();

        return redirect()
            ->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CheckIn  $checkIn
     * @return \Illuminate\Http\Response
     */
    public function show(CheckIn $checkIn)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CheckIn  $checkIn
     * @return \Illuminate\Http\Response
     */
    public function edit(CheckIn $checkIn)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CheckIn  $checkIn
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $zone, $checkin )
    {
        $checkIn = CheckIn::where( 'id', $checkin )
            ->where('zone_id', $zone )
            ->first();

        $checkIn->check_out = Carbon::now()->format('Y-m-d H:i:s');

        $checkIn->save();

        if($request->new_check_in){

            return redirect()->back();
            
        }else{

            return redirect()->route('dashboard');

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CheckIn  $checkIn
     * @return \Illuminate\Http\Response
     */
    public function destroy($zone, $checkIn)
    {
        $checkin = CheckIn::where( 'id', $checkIn )->first();

        $checkin->delete();

        return redirect()->route('checks', $zone )->with('msg', 'Se ha eliminado el check correctamente');
    }

    public function find_current_check_in( $agent, $zone )
    {

        $checkin = null;

        $current_check_in = $agent->check_in()
            ->where('zone_id', $zone->zone_id)
            ->where('check_out', null)
            ->first();

        if ($current_check_in) {
            $checkin = $current_check_in;
        }

        return $checkin;

    }

    public function find_check_in_not_close( $agent )
    {
        $checkin_not_close = null;

        $current_check_in = $agent->check_in()
            ->where('check_out', null)
            ->first();

        if ($current_check_in) {
            $checkin_not_close = $current_check_in;
        }

        return $checkin_not_close;
    }
}
