<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Zones\Category;
use App\Models\Zones\Message;
use App\Models\Zones\Zone;
use App\Models\Users\Agent;
use App\Models\Users\Contact;
use App\Models\Zones\ZoneAgent;
use App\Models\Zones\ZoneContact;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user   = auth()->user();

        $agents = null;

        if ( $user->role == 'admin' ) {
            
            $zones  = Zone::latest()->get();

            $agents = Agent::all();

        } else if ( $user->role == 'agent' ) {

            $zones = $user->agent->zones();

        } else if ( $user->role == 'contact' ){

            $zones = $user->contact->zones();
            
        }

        return view( 'dashboard', compact('zones', 'agents'));
        
    }

    public function get_chart_dashboard_data( Request $request){
        
        $user = auth()->user();
        $datasets   = [];
        $labels     = [];
        $type       = '';
        $start_date = date( 'Y-m-d', strtotime( $request->start_date ) );
        $end_date   = date( 'Y-m-d', strtotime( $request->end_date ) );

        if ( $request->chart == 'messages_category' || $request->chart == 'messages_agent_activity' ) {
            
            $type       = 'line';
            $categories = Category::all();
            
            foreach ($categories as $category) {
                $chart_data = [];
                $chart_data[ 'label' ] = $category->name;

                $date_helper = $start_date;
                while ($date_helper <= $end_date ) {

                    if ( $request->chart == 'messages_category' ) {

                        $count_messages = Message::where( 'zone_id', $request->zone )
                            ->whereDate( 'created_at', $date_helper )
                            ->where( 'category_id', $category->category_id )
                            ->get()
                            ->count();
                        
                    } else if ( $request->chart == 'messages_agent_activity' ){

                        $count_messages = Message::where( 'zone_id', $request->zone )
                            ->whereDate( 'created_at', $date_helper )
                            ->where( 'category_id', $category->category_id )
                            ->where( 'agent_id', $user->agent->agent_id )
                            ->get()
                            ->count();

                    }

                    $chart_data[ 'data' ][] = $count_messages;
                    
                    $labels[ $date_helper ] = $date_helper;
                    $date_helper = date( 'Y-m-d', strtotime( '+1 day '.$date_helper ) );
                }
            
                $chart_data[ 'borderColor' ]     = $category->color;
                $chart_data[ 'backgroundColor' ] = 'transparent';
                $chart_data[ 'borderWidth' ]     = 1;

                $datasets[] = $chart_data;
            }

        } else if ( $request->chart == 'messages_agents' ){

            $type       = 'bar';
            $zones      = ZoneAgent::where( 'agent_id', $request->agent )->get();

            foreach ($zones as $zone) {
                $chart_data = [];
                $chart_data[ 'label' ] = $zone->zone->name;

                $date_helper = $start_date;
                while ($date_helper <= $end_date ) {
                    $count_messages = Message::where( 'zone_id', $zone->zone_id )
                        ->whereDate( 'created_at', $date_helper )
                        ->where( 'agent_id', $request->agent )
                        ->get()
                        ->count();


                    $chart_data[ 'data' ][] = $count_messages;
                    
                    $labels[ $date_helper ] = $date_helper;
                    $date_helper = date( 'Y-m-d', strtotime( '+1 day '.$date_helper ) );
                }
            
                $chart_data[ 'borderColor' ]     = '#'.rand(0,9).rand(0,9).rand(0,9);
                $chart_data[ 'backgroundColor' ] = $chart_data[ 'borderColor' ];
                $chart_data[ 'borderWidth' ]     = 1;

                $datasets[] = $chart_data;
            }

        }

        $chart_response = [
            "type" => $type,
            "data" => [
                "labels" => array_values($labels),
                "datasets" => $datasets
            ],
            /*"options" => [
                "legend" => false
            ]*/
        ];

        return response()->json( $chart_response );
    }

    public function mark_notifications_as_read( $notification ){

        $user = auth()->user();

        $response = [];

        $notification = $user->unreadNotifications->where('id', $notification )->first();
        if ($notification) {

            $notification->markAsRead();

        }
        
        $response = [
            'result' => true,
        ];
            
        return response()->json( $response );
    }

    public function mark_all_notifications_as_read(){
        $user = auth()->user();
        
        if ($user->unreadNotifications->count() > 0) {

            $user->unreadNotifications->markAsRead();

            $msg = 'Todas las notificaciones fueron marcadas como leidas, si deseas revisarlas, puedes verificar los correos diarios que han sido enviados';
        } else {

            $msg = 'No hay notificaciones';

        }
            
        return redirect()
            ->back()
            ->with('msg', $msg);
    }

    public function notifications(){

        return view('notifications.index');

    }

}
