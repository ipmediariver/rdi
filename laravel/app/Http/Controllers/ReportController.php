<?php

namespace App\Http\Controllers;

use App\Models\Users\Agent;
use App\Models\Zones\Category;
use App\Models\Zones\Message;
use App\Models\Zones\Severity;
use App\Models\Zones\Zone;
use Illuminate\Http\Request;
use Validator;

class ReportController extends Controller
{

    public function index()
    {
        return view('reports.index');
    }

    public function generate_report(Request $request)
    {
        $this->validation($request);

        $response = [
            'activity_by_category'       => $this->activity_by_category($request),
            'activity_by_agent'          => $this->activity_by_agent($request),
            'total_activity_by_category' => $this->total_activity_by_category($request),
            'activity_by_schedule'       => $this->activity_by_schedule($request),
            'total_verified_messages'    => $this->total_verified_messages($request),
            'total_severity_messages'    => $this->total_severity_messages($request),
            'range'                      => [
                'report_start' => $request->report_start,
                'report_end'   => $request->report_end,
            ],
            'zone_id'                    => $request->zone_id,
        ];

        return response()->json($response);
    }

    public function download_report(Request $request)
    {
        $report = json_decode($request->report);

        $messages = Message::where('zone_id', $report->zone_id)
            ->whereDate('created_at', '>=', date('Y-m-d', strtotime($report->range->start)))
            ->whereDate('created_at', '<=', date('Y-m-d', strtotime($report->range->end)));

        if (isset($report->category_id)) {
            $messages = $messages->where('category_id', $report->category_id);
        }

        if (isset($report->verified)) {
            $messages = $messages->where('verified', $report->verified);
        }

        $messages = $messages->orderBy('created_at', 'DESC')->get();

        $zone = Zone::where('zone_id', $report->zone_id)->first();

        $data = [

            'report'   => $report,
            'messages' => $messages,
            'zone'     => $zone,

        ];

        $pdf = \PDF::loadView('reports.messages_export_report', $data);

        return $pdf->stream();

        // return view('reports.messages_export_report', compact('messages', 'report'));
    }

    public function activity_by_category(Request $request)
    {
        $report_start = date('Y-m-d', strtotime('-15 day'));
        $report_end   = date('Y-m-d');
        if (isset($request->report_start)) {
            $report_start = date('Y-m-d', strtotime($request->report_start));
        }

        if (isset($request->report_end)) {
            $report_end = date('Y-m-d', strtotime($request->report_end));
        }

        if (isset($request->zone_id)) {
            $zones = Zone::where('zone_id', $request->zone_id)->get();
        } else {
            $zones = Zone::get();
        }

        if (isset($request->category_id)) {
            $categories = Category::where('category_id', $request->category_id )->get();
        } else {
            $categories = Category::get()->sortBy('fullName');
        }

        $datasets = [];
        $labels   = [];

        foreach ($zones as $zone) {
            foreach ($categories as $category) {
                $date_helper      = $report_start;
                $dataset          = [];
                $dataset['label'] = $category->fullName;

                while ($date_helper <= $report_end) {

                    $messages = $zone->messages()
                            ->where('category_id', $category->category_id)
                            ->whereDate('created_at', $date_helper);

                    if (isset($request->verified)) {
                        $messages = $messages->where('verified', $request->verified);
                    }

                    $dataset['data'][] = $messages->count();

                    $dataset['borderColor'] = $category->color;

                    $dataset['backgroundColor'] = $category->color;

                    // $dataset['borderWidth'] = 1;

                    $dataset['fill'] = false;

                    $labels[$date_helper] = date('d/m/Y', strtotime($date_helper));

                    $date_helper = date('Y-m-d', strtotime('+1 day ' . $date_helper));
                }

                $datasets[] = $dataset;
            }
        }

        $options = [
            'legend' => [
                'display'  => true,
                'position' => 'top',
            ],
            'scales' => [
                'yAxes' => [
                    [
                        'ticks' => [
                            'beginAtZero' => true,
                        ],
                    ],
                ],
            ],
        ];

        $chart_data = $this->chart_structure(array_values($labels), $datasets, $options, 'line');

        return $chart_data;
    }

    public function total_activity_by_category(Request $request)
    {
        $report_start = date('Y-m-d', strtotime('-15 day'));
        $report_end   = date('Y-m-d');
        if (isset($request->report_start)) {
            $report_start = date('Y-m-d', strtotime($request->report_start));
        }

        if (isset($request->report_end)) {
            $report_end = date('Y-m-d', strtotime($request->report_end));
        }

        if (isset($request->zone_id)) {
            $zones = Zone::where('zone_id', $request->zone_id)->get();
        } else {
            $zones = Zone::get();
        }

        if (isset($request->category_id)) {
            $categories = Category::where('category_id', $request->category_id )->get();
        } else {
            $categories = Category::get()->sortBy('fullName');
        }

        $datasets = [];
        $labels   = [];

        foreach ($zones as $zone) {
            foreach ($categories as $category) {
                $messages = $zone->messages()
                    ->where('category_id', $category->category_id)
                    ->whereDate('created_at', '>=', $report_start)
                    ->whereDate('created_at', '<=', $report_end);

                if (isset($request->verified)) {
                    $messages = $messages->where('verified', $request->verified);
                }

                $data[] = $messages->count();

                $colors[] = $category->color;
                $labels[] = $category->fullName;
            }

            $datasets[] = [
                'data'            => $data,
                'backgroundColor' => $colors,
            ];
        }

        $options = [
            'legend' => [
                'display'  => true,
                'position' => 'bottom',
            ],
            /*'scales' => [
        'yAxes' =>[
        [
        'ticks' => [
        'beginAtZero' => true
        ]
        ]
        ]
        ]*/
        ];

        $chart_data = $this->chart_structure(array_values($labels), $datasets, $options, 'doughnut');

        return $chart_data;
    }

    public function total_severity_messages(Request $request)
    {
        $report_start = date('Y-m-d', strtotime('-15 day'));
        $report_end   = date('Y-m-d');
        if (isset($request->report_start)) {
            $report_start = date('Y-m-d', strtotime($request->report_start));
        }

        if (isset($request->report_end)) {
            $report_end = date('Y-m-d', strtotime($request->report_end));
        }

        if (isset($request->zone_id)) {
            $zones = Zone::where('zone_id', $request->zone_id)->get();
        } else {
            $zones = Zone::get();
        }

        $severities = Severity::get();
        $datasets   = [];
        $labels     = [];
        $data       = [];
        $colors     = [];

        foreach ($zones as $zone) {
            foreach ($severities as $severity) {
                $messages = $zone->messages()
                    ->whereDate('created_at', '>=', $report_start)
                    ->whereDate('created_at', '<=', $report_end)
                    ->where('severity_id', $severity->id);

                if (isset($request->category_id)) {
                    $messages = $messages->where('category_id', $request->category_id);
                }

                if (isset($request->verified)) {
                    $messages = $messages->where('verified', $request->verified);
                }

                $data[]                  = $messages->count();
                $colors[]                = $severity->color;
                $labels[$severity->name] = $severity->name;
            }
            $datasets[] = [
                'data'            => $data,
                'backgroundColor' => $colors,
            ];
        }

        $options = [
            'legend' => [
                'display'  => false,
                'position' => 'left',
            ],
            'scales' => [
                'yAxes' => [
                    [
                        'ticks' => [
                            'beginAtZero' => true,
                        ],
                    ],
                ],
            ],
        ];

        $chart_data = $this->chart_structure(array_values($labels), $datasets, $options, 'bar');

        return $chart_data;
    }

    public function activity_by_agent(Request $request)
    {
        $report_start = date('Y-m-d', strtotime('-15 day'));
        $report_end   = date('Y-m-d');
        if (isset($request->report_start)) {
            $report_start = date('Y-m-d', strtotime($request->report_start));
        }

        if (isset($request->report_end)) {
            $report_end = date('Y-m-d', strtotime($request->report_end));
        }

        if (isset($request->zone_id)) {
            $zones = Zone::where('zone_id', $request->zone_id)->get();
        } else {
            $zones = Zone::get();
        }

        $messages_agents = Message::select('agent_id')
            ->whereNotNull('agent_id')
            ->whereDate('created_at', '>=', $report_start)
            ->whereDate('created_at', '<=', $report_end);
        if (isset($request->zone_id)) {
            $messages_agents = $messages_agents->where('zone_id', $request->zone_id);
        }
        $messages_agents = $messages_agents->groupBy('agent_id')->get();

        $agents   = Agent::whereIn('agent_id', $messages_agents)->get();
        $datasets = [];
        $labels   = [];
        $data     = [];
        $colors   = [];

        foreach ($zones as $zone) {
            foreach ($agents as $agent) {
                $messages = $zone->messages()
                    ->where('agent_id', $agent->agent_id)
                    ->whereDate('created_at', '>=', $report_start)
                    ->whereDate('created_at', '<=', $report_end);

                if (isset($request->verified)) {
                    $messages = $messages->where('verified', $request->verified);
                }

                if (isset($request->category_id)) {
                    $messages = $messages->where('category_id', $request->category_id);
                }

                $data[] = $messages->count();
                $exa_color = (string) dechex(mt_rand(0, 255)) . dechex(mt_rand(0, 255)) . dechex(mt_rand(0, 255));

                $exa_color = (strlen($exa_color) == 6) ? $exa_color : substr($exa_color, 0, 3);

                $color                      = '#' . $exa_color;
                $colors[]                   = $color;
                $labels[$agent->user->name] = $agent->user->name;
            }
            $datasets[] = [
                'data'            => $data,
                'backgroundColor' => $colors,
            ];
        }

        $options = [
            'legend' => [
                'display'  => false,
                'position' => 'left',
            ],
            'scales' => [
                'yAxes' => [
                    [
                        'ticks' => [
                            'beginAtZero' => true,
                        ],
                    ],
                ],
            ],
        ];

        $chart_data = $this->chart_structure(array_values($labels), $datasets, $options, 'bar');

        return $chart_data;
    }

    public function activity_by_schedule(Request $request)
    {
        $report_start = date('Y-m-d', strtotime('-15 day'));
        $report_end   = date('Y-m-d');
        $schedules    = [
            'Matutino'   => [
                'color' => '#81BEF7',
                'start' => '08:00:00',
                'end'   => '15:59:59',
            ],
            'Vespertino' => [
                'color' => '#FAAC58',
                'start' => '16:00:00',
                'end'   => '23:59:59',
            ],
            'Nocturno'   => [
                'color' => '#585858',
                'start' => '00:00:00',
                'end'   => '07:59:59',
            ],
        ];
        if (isset($request->report_start)) {
            $report_start = date('Y-m-d', strtotime($request->report_start));
        }

        if (isset($request->report_end)) {
            $report_end = date('Y-m-d', strtotime($request->report_end));
        }

        if (isset($request->zone_id)) {
            $zones = Zone::where('zone_id', $request->zone_id)->get();
        } else {
            $zones = Zone::get();
        }

        $datasets = [];
        $labels   = [];

        foreach ($zones as $zone) {
            foreach ($schedules as $name => $schedule) {
                $date_helper      = $report_start;
                $dataset          = [];
                $dataset['label'] = $name;

                while ($date_helper <= $report_end) {

                    $messages = $zone->messages()
                        ->where('created_at', '>=', date('Y-m-d H:i:s', strtotime($date_helper . ' ' . $schedule['start'])))
                        ->where('created_at', '<=', date('Y-m-d H:i:s', strtotime($date_helper . ' ' . $schedule['end'])));

                    if (isset($request->category_id)) {
                        $messages = $messages->where('category_id', $request->category_id);
                    }

                    if (isset($request->verified)) {
                        $messages = $messages->where('verified', $request->verified);
                    }

                    if (isset($request->debug)) {
                        echo $messages->count() . ' ' . $name . ' ' . date('Y-m-d H:i:s', strtotime($date_helper . ' ' . $schedule['start'])) . ' ' . date('Y-m-d H:i:s', strtotime($date_helper . ' ' . $schedule['end'])) . '<br>';
                    }

                    $dataset['data'][] = $messages->count();

                    $dataset['borderColor'] = $schedule['color'];

                    $dataset['backgroundColor'] = $schedule['color'];

                    $dataset['fill'] = false;

                    $labels[$date_helper] = date('d/m/Y', strtotime($date_helper));

                    $date_helper = date('Y-m-d', strtotime('+1 day ' . $date_helper));
                }

                $datasets[] = $dataset;
            }
        }

        if (isset($request->debug)) {
            exit();
        }

        $options = [
            'legend' => [
                'display'  => true,
                'position' => 'top',
            ],
            'scales' => [
                'yAxes' => [
                    [
                        'ticks' => [
                            'beginAtZero' => true,
                        ],
                    ],
                ],
            ],
        ];

        $chart_data = $this->chart_structure(array_values($labels), $datasets, $options, 'line');

        return $chart_data;
    }

    public function total_verified_messages(Request $request)
    {
        $report_start = date('Y-m-d', strtotime('-15 day'));
        $report_end   = date('Y-m-d');
        if (isset($request->report_start)) {
            $report_start = date('Y-m-d', strtotime($request->report_start));
        }

        if (isset($request->report_end)) {
            $report_end = date('Y-m-d', strtotime($request->report_end));
        }

        if (isset($request->zone_id)) {
            $zones = Zone::where('zone_id', $request->zone_id)->get();
        } else {
            $zones = Zone::get();
        }

        $datasets = [];
        $labels   = [];
        foreach ($zones as $zone) {
            if (isset($request->verified)) {
                
                if ($request->verified == 0) {
                    #messages verified
                    $messages_no_verified = $zone->messages()
                        ->where('verified', '0')
                        ->whereDate('created_at', '>=', $report_start)
                        ->whereDate('created_at', '<=', $report_end);
                    if (isset($request->category_id)) {
                        $messages_no_verified = $messages_no_verified->where('category_id', $request->category_id);
                    }


                    $data[] = $messages_no_verified->count();
                    $colors[] = '#FB7171';
                    $labels[] = 'No verficado';
                }

                if ($request->verified == 1) {
                    #message verified
                    $messages_verified = $zone->messages()
                        ->where('verified', '1')
                        ->whereDate('created_at', '>=', $report_start)
                        ->whereDate('created_at', '<=', $report_end);
                    
                    if (isset($request->category_id)) {
                        $messages_verified = $messages_verified->where('category_id', $request->category_id);
                    }

                    $data[] = $messages_verified->count();
                    $colors[] = '#5AE18C';
                    $labels[] = 'Incidencias';
                }
            } else {
                #messages verified
                $messages_no_verified = $zone->messages()
                    ->where('verified', '0')
                    ->whereDate('created_at', '>=', $report_start)
                    ->whereDate('created_at', '<=', $report_end);
                if (isset($request->category_id)) {
                    $messages_no_verified = $messages_no_verified->where('category_id', $request->category_id);
                }


                $data[] = $messages_no_verified->count();
                $colors[] = '#FB7171';
                $labels[] = 'No verficados';

                #message verified
                $messages_verified = $zone->messages()
                    ->where('verified', '1')
                    ->whereDate('created_at', '>=', $report_start)
                    ->whereDate('created_at', '<=', $report_end);
                
                if (isset($request->category_id)) {
                    $messages_verified = $messages_verified->where('category_id', $request->category_id);
                }

                $data[] = $messages_verified->count();
                $colors[] = '#5AE18C';
                $labels[] = 'Verificados';
            }
        }

        $datasets[] = [
            'data'            => $data,
            'backgroundColor' => $colors,
        ];

        $options = [
            'legend' => [
                'display'  => false,
                'position' => 'right',
            ],
            'scales' => [
                'yAxes' => [
                    [
                        'ticks' => [
                            'beginAtZero' => true,
                        ],
                    ],
                ],
            ],
        ];

        $chart_data = $this->chart_structure(array_values($labels), $datasets, $options, 'bar');

        return $chart_data;
    }

    public function chart_structure($labels = array(), $datasets = array(), $options = array(), $type = 'line')
    {

        $chart_structure = [
            "type" => $type,
            "data" => [
                "labels"   => $labels,
                "datasets" => $datasets,
            ],
        ];

        if ($options) {
            $chart_structure['options'] = $options;
        }

        return $chart_structure;
    }

    public function validation($request)
    {
        $validate_cond = [
            'zone_id'      => 'nullable|exists:zones,zone_id',
            'category_id'  => 'nullable|exists:categories,category_id',
            'verified'     => 'nullable',
            'report_start' => 'required|date_format:"d-m-Y"',
            'report_end'   => 'required|date_format:"d-m-Y"|after_or_equal:report_start',
        ];

        $validate_msgs = [
            'zone_id.exists'            => 'La zona seleccionada no existe en la base de datos',
            'category_id.exists'        => 'La categoría seleccionada no existe en la base de datos',
            'report_start.required'     => 'La fecha de inicio es requerida',
            'report_start.date_format'  => 'La fecha de inicio no cumple con el formato d-m-Y',
            'report_end.required'       => 'La fecha final es requerida',
            'report_end.date_format'    => 'La fecha final no cumple con el formato d-m-Y',
            'report_end.after_or_equal' => 'Las fechas no cumplen un rango determinado, por favor, indica una fecha de inicio menor a la fecha final',
        ];

        $validate = Validator::make($request->all(), $validate_cond, $validate_msgs)->validate();
    }
}
