<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\Users\Agent;
use App\Models\Users\User;
use App\Models\Zones\Zone;
use Illuminate\Http\Request;
use App\Notifications\UserRegister;
use Notification;
use Validator as Validate;
use Illuminate\Validation\Rule;

class AgentsController extends Controller
{

    public function validation(Request $request, $user = null)
    {
        app('App\Http\Controllers\UsersController')->validation( $request, $user );
    }

    public function __construct(){

        $this->middleware('admin', ['except' => ['show']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $agents = Agent::orderBy('created_at', 'desc')->paginate(15);

        return view('users.agents.index', compact('agents'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.agents.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validation($request);
        
        // $password = str_random(6);
        $password = 'secret';

        $user = new User();

        $user->name     = $request->name;
        $user->username = $request->username;
        $user->email    = $request->email;
        $user->password = \Hash::make( $password );
        $user->role     = 'agent';
        $user->save();

        $agent           = new Agent();
        $agent->agent_id = 'agent-' . uniqid();
        $agent->user_id  = $user->id;

        $agent->save();

        $new_agent_msg = 'Se ha creado un agente correctamente.';

        $notification_data = [
            'user'     => $user,
            'password' => $password,
        ];

        Notification::send( $user, new UserRegister( $notification_data ) );

        $new_request = new Request([
            'agent' => $agent->agent_id,
        ]);

        $zones = Zone::select('zone_id')->get();

        foreach ($zones as $zone) {

            app('\App\Http\Controllers\Zones\AgentsController')->store($new_request, $zone->zone_id );

        }

        if ($request->rel_zone) {

            return redirect()
                ->route('zone-agents', $request->rel_zone)
                ->with('msg', $new_agent_msg);

        }

        return redirect()
            ->route('show-agent', $agent->agent_id)
            ->with('msg', $new_agent_msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $agent = Agent::where('agent_id', $id)->firstOrFail();

        return view('users.agents.show', compact('agent'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $agent = Agent::where('agent_id', $id)->firstOrFail();

        return view('users.agents.edit', compact('agent'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $agent = Agent::where('agent_id', $id)->firstOrFail();

        $this->validation($request, $agent->user );

        $agent->user->name     = $request->name;
        $agent->user->username = $request->username;
        $agent->user->email    = $request->email;

        if (isset( $request->password ) ) {

            $agent->user->password = \Hash::make( $request->password );

        } 

        $agent->user->save();

        return redirect()
            ->route('show-agent', $agent->agent_id)
            ->with('msg', 'El agente ha sido modificado correctamente');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $agent = Agent::where('agent_id', $id)->firstOrFail();

        $agent->user->delete();

        $agent->assignedZones()->delete();

        $agent->delete();

        return redirect()
            ->route('agents')
            ->with('msg', 'El agente ha sido eliminado correctamente');

    }
}
