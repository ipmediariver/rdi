<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\Users\Contact;
use App\Models\Users\User;
use App\Models\Zones\Zone;
use Illuminate\Http\Request;
use App\Notifications\UserRegister;

use Notification;
use Validator as Validate;
use Illuminate\Validation\Rule;

class ContactsController extends Controller
{

    public function __construct(){

        $this->middleware('admin')->except(['show']);

    }

    public function validation(Request $request, $user = null)
    {
        app('App\Http\Controllers\UsersController')->validation( $request, $user );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $contacts = Contact::latest()->paginate(15);

        return view('users.contacts.index', compact('contacts'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.contacts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validation($request);        

        // $password = str_random(6);
        $password = 'secret';

        $user = new User();

        $user->name     = $request->name;
        $user->username = $request->username;
        $user->email    = $request->email;
        $user->password = \Hash::make( $password );
        $user->role     = 'contact';
        $user->save();

        $contact             = new Contact();
        $contact->contact_id = 'contact-' . uniqid();
        $contact->user_id    = $user->id;

        $contact->save();

        $new_contact_msg = 'Se ha creado un contacto correctamente.';

        $notification_data = [
            'user'     => $user,
            'password' => $password,
        ];

        Notification::send( $user, new UserRegister( $notification_data ) );

        if($request->rel_zone){

            $zone = Zone::where('zone_id', $request->rel_zone)->firstOrFail();

            $new_request = new Request([
                'contact' => $contact->contact_id
            ]);

            app('\App\Http\Controllers\Zones\ContactsController')->store($new_request, $request->rel_zone);

            return redirect()
                ->route('zone-contacts', $request->rel_zone)
                ->with('msg', $new_contact_msg);    

        }else{

            return redirect()
                ->route('show-contact', $contact->contact_id)
                ->with('msg', $new_contact_msg);
        
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $contact = Contact::where('contact_id', $id)->with('categories_contact')->firstOrFail();


        if(request()->ajax()){

            return response()->json($contact);

        }else{

            return view('users.contacts.show', compact('contact'));
            
        }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $contact = Contact::where('contact_id', $id)->firstOrFail();

        return view('users.contacts.edit', compact('contact'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $contact = Contact::where('contact_id', $id)->firstOrFail();

        $this->validation($request, $contact->user );

        $contact->user->name     = $request->name;
        $contact->user->username = $request->username;
        $contact->user->email    = $request->email;

        if (isset( $request->password ) ) {

            $contact->user->password = \Hash::make( $request->password );

        } 

        $contact->user->save();

        return redirect()
            ->route('show-contact', $contact->contact_id)
            ->with('msg', 'El contacto ha sido modificado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = Contact::where('contact_id', $id)->firstOrFail();

        $contact->user->delete();

        $contact->assignedZones()->delete();

        $contact->delete();

        return redirect()
            ->route('contacts')
            ->with('msg', 'El contacto ha sido eliminado correctamente');
    }
}
