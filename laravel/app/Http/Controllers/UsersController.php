<?php

namespace App\Http\Controllers;

use App\Models\Users\User;
use App\Notifications\UserRegister;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Notification;
use Validator as Validate;

class UsersController extends Controller
{

    public function validation(Request $request, $user = null)
    {
        $validate_msgs = [
            'name.required'      => 'El campo Nombre es requerido',
            'username.required'  => 'El campo Nombre de usuario es requerido',
            'username.unique'    => 'El usuario ' . $request->username . ' ya existe, intenta con otro',
            'username.alpha_num' => 'El usuario solo debe tener números y letras',
            'email.required'     => 'El campo E-mail es requerido',
            'email.unique'       => 'El correo ' . $request->email . ' ya existe, intenta con otro',
        ];

        $validate_cond = [
            'name'     => 'required',
            'username' => 'required|alpha_num|unique:users',
            'email'    => [

                'required',
                $user ? Rule::unique('users')->ignore($user->id) : 'unique:users',

            ],
        ];

        if (isset($request->password)) {

            $validate_msgs['password.required']              = 'El campo contraseña nueva es requerido';
            $validate_msgs['password.min']                   = 'El campo contraseña nueva debe tener como mínimo, 6 carácteres';
            $validate_msgs['password.confirmed']             = 'Las contraseñas no coinciden, intenta nuevamente';
            $validate_msgs['password_confirmation.required'] = 'El campo confirmar contraseña es requerido';
            $validate_msgs['password_confirmation.min']      = 'El campo confirmar contraseña debe tener como mínimo, 6 carácteres';

            $validate_cond['password']              = 'required|min:6|confirmed';
            $validate_cond['password_confirmation'] = 'required|min:6';

        }

        $validate = Validate::make($request->all(), $validate_cond, $validate_msgs)->validate();

    }

    public function index()
    {
        $users = User::where('role', 'admin')->paginate(15);

        return view('users.admins.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.admins.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validation($request);

        // $password = str_random(6);
        $password = 'secret';

        $user = new User();

        $user->name     = $request->name;
        $user->username = $request->username;
        $user->email    = $request->email;
        $user->password = \Hash::make($password);
        $user->role     = 'admin';
        $user->save();

        $new_agent_msg = 'Se ha creado un perfil de administrador correctamente.';

        $notification_data = [
            'user'     => $user,
            'password' => $password,
        ];

        Notification::send($user, new UserRegister($notification_data));

        return redirect()
            ->route('admins')
            ->with('msg', 'El administrador fue creado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::where('id', $id)->firstOrFail();

        return view('users.admins.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::where('id', $id)->firstOrFail();

        return view('users.admins.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::where('id', $id)->firstOrFail();

        $this->validation($request, $user);

        $user->name     = $request->name;
        $user->username = $request->username;
        $user->email    = $request->email;

        if (isset($request->password)) {

            $user->password = \Hash::make($request->password);

        }

        $user->save();

        return redirect()
            ->back()
            ->with('msg', 'El administrador ha sido modificado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $user = User::where('id', $id)->firstOrFail();

        $user->delete();

        return redirect()
            ->route('admins')
            ->with('msg', 'El administrador ha sido eliminado correctamente');

    }

    public function profile()
    {

        $user = auth()->user();

        return view('users.profile', compact('user'));

    }

    public function update_password(Request $request)
    {

        $validate_msgs = [
            'password.required'              => 'El campo contraseña nueva es requerido',
            'password.min'                   => 'El campo contraseña nueva debe tener como mínimo, 6 carácteres',
            'password.confirmed'             => 'Las contraseñas no coinciden, intenta nuevamente',
            'password_confirmation.required' => 'El campo confirmar contraseña es requerido',
            'password_confirmation.min'      => 'El campo confirmar contraseña debe tener como mínimo, 6 carácteres',
        ];

        $validate = Validate::make($request->all(), [
            'password'              => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
        ], $validate_msgs)->validate();

        $user = auth()->user();

        $user->password = bcrypt($request->password);
        $user->save();

        return redirect()
            ->route('profile')
            ->with('msg', 'La contraseña fue actualizada correctamente');
    }

}
