<?php

namespace App\Http\Controllers\Zones;

use App\Http\Controllers\Controller;
use App\Models\Users\Agent;
use App\Models\Zones\Zone;
use App\Models\Zones\ZoneAgent;
use Illuminate\Http\Request;

use App\Notifications\UserAsignZone;
use Notification;

class AgentsController extends Controller
{


    public function __construct(){


        $this->middleware('admin');


    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($zone)
    {

        $zone = Zone::where('zone_id', $zone)->firstOrFail();

        $agents = $zone->agents()->latest()->paginate(10);

        return view('zones.agents.index', compact('zone', 'agents'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $zone)
    {

        $zone = Zone::where('zone_id', $zone)->firstOrFail();

        $agent = Agent::where('agent_id', $request->agent)->firstOrFail();

        $alreadyAssigned = ZoneAgent::where('zone_id', $zone->zone_id)
            ->where('agent_id', $agent->agent_id)
            ->first();

        if (!$alreadyAssigned) {

            $assign = new ZoneAgent();

            $assign->zone_id  = $zone->zone_id;
            $assign->agent_id = $agent->agent_id;

            $assign->save();

            $notification_data = [
                'subject' => 'Agente Asignado a zona',
                'zone'    => $zone,
                'user'   => $agent->user,
            ];

            Notification::send( $agent->user, new UserAsignZone( $notification_data ) );

            return redirect()
                ->back()
                ->with('msg', 'Se ha asignado el agente correctamente');

        } else {

            return redirect()
                ->back()
                ->with('msg', 'El agente ya se encuentra asignado a esta zona');

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($zone, $agent)
    {
        $assignedAgent = ZoneAgent::where('zone_id', $zone)
            ->where('agent_id', $agent)
            ->firstOrFail();

        $assignedAgent->delete();

        return redirect()
            ->back()
            ->with('msg', 'El agente ha sido eliminado de esta zona.');
    }
}
