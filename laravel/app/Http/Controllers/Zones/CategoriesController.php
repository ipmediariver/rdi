<?php

namespace App\Http\Controllers\Zones;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Zones\Category;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        

        $categories = Category::whereNull('parent_id')->latest()->get();

        return view('categories.index', compact('categories'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = new Category();

        $category->name = $request->name;
        $category->color = $request->color;
        $category->category_id = 'category-' . uniqid();

        if (isset($request->parent)) {
            $category->parent_id = $request->parent;
        }

        $category->save();

        return redirect()->route('show-category', $category->category_id)->with('msg', 'Se ha creado una nueva categoría correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($category)
    {
        
        $category = Category::where('category_id', $category)->firstOrFail();

        return view('categories.show', compact('category'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($category)
    {
        
        $category = Category::where('category_id', $category)->firstOrFail();

        return view('categories.edit', compact('category'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $category)
    {
        
        $category = Category::where('category_id', $category)->firstOrFail();

        $category->update($request->only(['name', 'color']));

        return redirect()->back()->with('msg', 'La categoría ha sido actualizada correctamente');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($category)
    {
        $category = Category::where('category_id', $category)->firstOrFail();

        $category->delete();

        return redirect()->route('categories')->with('msg', 'La categoría ha sido eliminada correctamente');
    }
}
