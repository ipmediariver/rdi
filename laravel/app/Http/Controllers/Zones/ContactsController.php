<?php

namespace App\Http\Controllers\Zones;

use App\Http\Controllers\Controller;
use App\Models\Users\Contact;
use App\Models\Zones\Zone;
use App\Models\Zones\Category;
use App\Models\Zones\ZoneContact;
use Illuminate\Http\Request;

use App\Notifications\UserAsignZone;
use Notification;

class ContactsController extends Controller
{


    public function __construct(){

        $this->middleware('admin', ['except' => ['index']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($zone)
    {

        $zone = Zone::where('zone_id', $zone)->firstOrFail();

        $contacts = $zone->contacts()->latest()->get();

        $categories = Category::get();

        $checkin  = null;
        
        if ( auth()->user()->role == 'agent' ) {

            $agent = auth()->user()->agent;

            $checkin = app('App\Http\Controllers\CheckInController')->find_current_check_in( $agent, $zone );

        }

        return view('zones.contacts.index', compact( 'zone', 'contacts', 'checkin', 'categories' ) );

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $zone)
    {

        $zone = Zone::where('zone_id', $zone)->firstOrFail();

        $contact = Contact::where('contact_id', $request->contact)->firstOrFail();

        $alreadyAssigned = ZoneContact::where('zone_id', $zone->zone_id)
            ->where('contact_id', $contact->contact_id)
            ->first();

        if (!$alreadyAssigned) {

            $assign = new ZoneContact();

            $assign->zone_id    = $zone->zone_id;
            $assign->contact_id = $contact->contact_id;

            $assign->save();

            $notification_data = [
                'subject' => 'Contacto Asignado a zona',
                'zone'    => $zone,
                'user'   => $contact->user,
            ];

            Notification::send( $contact->user, new UserAsignZone( $notification_data ) );


            return redirect()
                ->back()
                ->with('msg', 'Se ha asignado el contacto correctamente');

        } else {

            return redirect()
                ->back()
                ->with('msg', 'El contacto ya se encuentra asignado a esta zona');

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($zone, $contact)
    {

        $assignedContact = ZoneContact::where('zone_id', $zone)
            ->where('contact_id', $contact)
            ->firstOrFail();

        $assignedContact->delete();

        return redirect()
            ->back()
            ->with('msg', 'El contacto ha sido eliminado de esta zona.');
    }
}
