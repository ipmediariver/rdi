<?php

namespace App\Http\Controllers\Zones;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Zones\Department;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::paginate(15);

        return view( 'zones.departments.index', compact( 'departments' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'zones.departments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $department = new Department();

        $department->department_id = 'department-' . uniqid();
        $department->name          = $request->name;
        $department->description   = $request->description;

        $department->save();

        return redirect()
            ->route( 'departments' )
            ->with('msg', 'Se ha creado un nuevo departamento correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($department)
    {
        $department = Department::where( 'id', $department )->first();

        return view( 'zones.departments.show', compact( 'department' ) );    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($department)
    {
        $department = Department::where( 'id', $department )->first();

        return view( 'zones.departments.edit', compact( 'department' ) ); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $department)
    {
        $department = Department::where( 'id', $department )->first();

        $department->name        = $request->name;
        $department->description = $request->description;

        $department->update();

        return redirect()
            ->route( 'departments' )
            ->with('msg', 'Se ha actualizado el departamento correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($department)
    {
        $department = Department::where( 'id', $department )->first();

        $department->delete();

        return redirect()
            ->route( 'departments' )
            ->with('msg', 'Se ha eliminado un departamento correctamente');
    }
}
