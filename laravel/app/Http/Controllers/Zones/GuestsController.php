<?php

namespace App\Http\Controllers\Zones;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Zones\Guest;
use App\Models\Zones\Zone;
use App\Models\Zones\Department;
use Carbon\Carbon;

use Image;
use Notification;
use Validator;

class GuestsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($zone)
    {

        $zone = Zone::where('zone_id', $zone)->firstOrFail();

        $guests = $zone->guests()->orderBy('start_time', 'desc')->paginate(15);

        $checkin  = null;
        
        if ( auth()->user()->role == 'agent' ) {

            $agent = auth()->user()->agent;

            $checkin = app('App\Http\Controllers\CheckInController')->find_current_check_in( $agent, $zone );

        }

        return view('zones.guests.index', compact('zone', 'guests', 'checkin'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($zone)
    {

        $zone = Zone::where('zone_id', $zone)->first();

        $departments = Department::all();

        $checkin  = null;
        
        if ( auth()->user()->role == 'agent' ) {

            $agent = auth()->user()->agent;

            $checkin = app('App\Http\Controllers\CheckInController')->find_current_check_in( $agent, $zone );

        }

        return view('zones.guests.create', compact( 'zone', 'departments', 'checkin' ) );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $zone)
    {

        $zone = Zone::where('zone_id', $zone)->firstOrFail();

        $guest = new Guest($request->except('_token'));
        
        $guest->zone_id = $zone->zone_id;

        $guest->guest_id = 'guest-' . uniqid();

        $guest->start_time = date('Y-m-d H:i:s', strtotime( date('Y-m-d').$request->start_time )  );

        $img = $request->finger_print;

        if ($img) {
            $msg = ['img.mimes' => 'Las imagenes deben ser .jpeg o .png'];

            $validateImg = Validator::make( ['img' => $img], [ 'img' => 'mimes:jpeg,bmp,png', 
            ], $msg)->validate();

            $imgName = 'img-' . uniqid() . '.jpg';

            $path = 'uploads5a186f377a231' . '/';

            $saveImg = Image::make($img->getRealPath())->resize(null, 300, function ($constraint) {

                $constraint->aspectRatio();

            });

            $saveImg->save($path . $imgName);

            $guest->finger_print = $path . $imgName;
        }

        $guest->save();

        return redirect()
            ->route('guests', $zone->zone_id)
            ->with('msg', 'La información de la visita ha sido guardada correctamente');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($zone, $id)
    {

        $zone = Zone::where('zone_id', $zone)->firstOrFail();

        $guest = Guest::where('guest_id', $id)->firstOrFail();

        $checkin  = null;
        
        if ( auth()->user()->role == 'agent' ) {

            $agent = auth()->user()->agent;

            $checkin = app('App\Http\Controllers\CheckInController')->find_current_check_in( $agent, $zone );

        }

        return view('zones.guests.show', compact('zone', 'guest', 'checkin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($zone, $id)
    {

        $zone = Zone::where('zone_id', $zone)->firstOrFail();
        
        $departments = Department::all();


        $guest = Guest::where('guest_id', $id)->firstOrFail();

        $checkin  = null;
        
        if ( auth()->user()->role == 'agent' ) {

            $agent = auth()->user()->agent;

            $checkin = app('App\Http\Controllers\CheckInController')->find_current_check_in( $agent, $zone );

        }

        return view('zones.guests.edit', compact('departments', 'zone', 'guest', 'checkin'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $zone, $id)
    {
        
        $zone = Zone::where('zone_id', $zone)->firstOrFail();


        $guest = Guest::where('guest_id', $id)->firstOrFail();

        $before_start_date = explode(' ', $guest->start_time );
        $before_end_date = explode(' ', $guest->end_time );

        $guest->start_time = date( 'Y-m-d', strtotime( $before_start_date[0] ) ).' '.date( 'H:i:s', strtotime( $request->start_time ) );
        $guest->end_time   = date( 'Y-m-d', strtotime( $before_end_date[0] ) ).' '.date( 'H:i:s', strtotime( $request->end_time ) );

        $guest->update($request->except('_token'));


        return redirect()
            ->route('show-guest', ['zone' => $zone->zone_id, 'guest' => $guest->guest_id])
            ->with('msg', 'La información de la visita ha sido actualizada');

    }


    public function set_end_time_on_each_guest(Request $request, $zone, $id){


        $zone = Zone::where('zone_id', $zone)->firstOrFail();


        $guest = Guest::where('guest_id', $id)->firstOrFail();


        $time = Carbon::now();


        $guest->update([

            'end_time' => $time

        ]);

        return redirect()->back()->with('msg', 'Se ha establecido la hora de salida correctamente');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($zone, $id)
    {

        $zone = Zone::where('zone_id', $zone)->firstOrFail();

        $guest = Guest::where('guest_id', $id)->firstOrFail();

        $guest->delete();

        return redirect()
            ->route('guests', $zone->zone_id)
            ->with('msg', 'El registro de la visita ha sido eliminado correctamente');

    }
}
