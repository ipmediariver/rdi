<?php

namespace App\Http\Controllers\Zones;

use App\Http\Controllers\Controller;
use App\Models\Users\User;
use App\Models\Zones\Comment;
use App\Models\Zones\Message;
use App\Models\Zones\Zone;
use App\Models\Zones\ZoneCategoryContact;
use App\Models\Zones\Category;
use App\Notifications\NewComment;
use App\Notifications\NewMessage;
use App\Notifications\VerifiedMessage;
use App\Notifications\HighSeverityMessage;
use App\Notifications\SendMessageZoneContactByCategory;
use Illuminate\Http\Request;
use Image;
use Notification;
use Validator;

class MessagesController extends Controller
{

    public function __construct()
    {

        $this->middleware('admin', ['only' => ['edit', 'update', 'destroy']]);

        $this->middleware('assigned_zone');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($zone)
    {

        $zone = Zone::where('zone_id', $zone)->firstOrFail();

        $user = auth()->user();

        $message = null;

        $checkin          = null;
        $check_not_close = null;

        if ($user->role == 'admin') {

            $messages = $zone->messages()->latest()->paginate(12);

        } elseif ($user->role == 'agent') {

            $agent = auth()->user()->agent;

            $checkin = app('App\Http\Controllers\CheckInController')->find_current_check_in($agent, $zone);

            $check_not_close = app('App\Http\Controllers\CheckInController')->find_check_in_not_close($agent);

            $messages = $zone->messages()->latest()->paginate(12);

        } elseif ($user->role == 'contact') {

            $messages = $zone->messages()->where('verified', 1)->latest()->paginate(12);

        }

        return view('zones.messages.index', compact('zone', 'messages', 'checkin', 'check_not_close'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($zone)
    {
        $zone = Zone::where('zone_id', $zone)->firstOrFail();

        $checkin = null;

        $categories = Category::get()->sortBy('fullName');

        if (auth()->user()->role == 'agent') {

            $agent = auth()->user()->agent;

            $checkin = app('App\Http\Controllers\CheckInController')->find_current_check_in($agent, $zone);

        }

        return view('zones.messages.create', compact('zone', 'checkin', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $zone)
    {

        $user = auth()->user();

        $zone = Zone::where('zone_id', $zone)->firstOrFail();

        $agent_id = null;

        if ($user->role == 'agent') {

            $agent_id = $user->agent->agent_id;

        } else {

            $agent_id = 'Admin';

        }


        $this->validation($request);


        $message = new Message();

        $message->zone_id     = $zone->zone_id;
        $message->agent_id    = $agent_id;
        $message->message_id  = 'msg-' . uniqid();
        $message->subject     = $request->subject;
        $message->description = $request->description;
        $message->category_id = $request->category_id;
        $message->severity_id = $request->severity_id;

        if ($request->images) {

            $message->images = json_encode(
                $this->store_uploaded_images_for_each_message($request->images)
            );

        }

        $message->save();

        $admins = User::where('role', 'admin')->get();

        $notification_data = [

            'type'    => 'Nuevo Mensaje',
            'author'  => $user,
            'message' => $message,

        ];

        Notification::send($admins, new NewMessage($notification_data));

        if ($message->get_severity->name == 'Alta') {
            $data = [
                'message' => $message,
            ];

            Notification::send( $admins, new HighSeverityMessage( $data ) );
        }

        return redirect()
            ->route('show-message', [
                'zone'    => $zone->zone_id,
                'message' => $message->message_id,
            ])
            ->with('msg', 'Se ha publicado el mensaje correctamente');

    }

    public function store_uploaded_images_for_each_message($images)
    {

        $images_collection = [];

        foreach ($images as $img) {

            $msg = ['img.mimes' => 'Las imagenes deben ser .jpeg o .png'];

            $validateImg = Validator::make(['img' => $img], [
                'img' => 'mimes:jpeg,bmp,png',
            ], $msg)->validate();

            $imgName = 'img-' . uniqid() . '.jpg';

            $path = 'uploads7e8r2t58d' . '/';

            $saveImg = Image::make($img->getRealPath())->resize(null, 500, function ($constraint) {

                $constraint->aspectRatio();

            });

            $saveImg->save($path . $imgName);

            array_push($images_collection, asset($path . $imgName));

        }

        return $images_collection;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $zone, $message)
    {

        $zone = Zone::where('zone_id', $zone)->firstOrFail();

        $message = Message::where('message_id', $message)->firstOrFail();

        $comments = $message->ajax_comments();

        if ($request->ajax()) {

            return response()->json($comments);

        } else {

            $checkin = null;

            if (auth()->user()->role == 'agent') {

                $agent = auth()->user()->agent;

                $checkin = app('App\Http\Controllers\CheckInController')->find_current_check_in($agent, $zone);

            }

            return view('zones.messages.show', compact('zone', 'message', 'comments', 'checkin'));

        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($zone, $message)
    {

        $zone = Zone::where('zone_id', $zone)->firstOrFail();

        $message = Message::where('message_id', $message)->firstOrFail();

        $categories = Category::get()->sortBy('fullName');

        return view('zones.messages.edit', compact('zone', 'message', 'categories'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $zone, $message)
    {
        $this->validation($request);
        
        $zone = Zone::where('zone_id', $zone)->firstOrFail();

        $message = Message::where('message_id', $message)->firstOrFail();

        $message->update([
            'images' => $request->current_images,
        ]);

        if ($request->images) {

            $new_images_added_to_message = $this->store_uploaded_images_for_each_message($request->images);

            $current_images = json_decode($message->images);

            $merge_new_images_into_current_images = json_encode(array_merge($new_images_added_to_message, $current_images));

            $message->update([
                'images' => $merge_new_images_into_current_images,
            ]);

        }

        $message->update([

            'subject'     => $request->subject,
            'description' => $request->description,
            'category_id' => $request->category_id,
            'severity_id' => $request->severity_id,

        ]);

        return redirect()
            ->route('show-message', ['zone' => $zone->zone_id, 'message' => $message->message_id])
            ->with('msg', 'El mensaje ha sido actualizado correctamente');

    }

    public function validate_message($zone, $message)
    {

        $zone = Zone::where('zone_id', $zone)->firstOrFail();

        $message = Message::where('message_id', $message)->firstOrFail();

        $message->update([

            'verified' => 1,

        ]);

        $message_link = route('show-message', ['zone' => $zone->zone_id, 'message' => $message->message_id]);

        $author = $message->author ? $message->author->user : auth()->user();

        $notification_to_author = [

            'notification' => 'Tu mensaje ha sido verificado.',
            'link'         => $message_link,

        ];

        $author->notify(new VerifiedMessage($notification_to_author));

        $zone_contacts = $message->zone->contacts;

        $notification_to_contacts = [

            'notification' => 'Tienes un nuevo mensaje en la zona ' . $zone->name,
            'link'         => $message_link,

        ];

        foreach ($zone_contacts as $assigned) {

            $assigned->contact->user->notify(new VerifiedMessage($notification_to_contacts));

        }

        //new notifications to contact by categories
        $zone_contact_categories = ZoneCategoryContact::where('zone_id', $zone->zone_id)
                ->where('category_id', $message->category_id)
            ->get();

        if ($zone_contact_categories->count() > 0) {
            foreach ($zone_contact_categories as $zone_contact_category) {
                $zone_contact_category->contact->user->notify( 
                    new SendMessageZoneContactByCategory(
                        $zone_contact_category->category, 
                        $message 
                    ) );
            }
        }

        return redirect()
            ->back()
            ->with('msg', 'El mensaje ha sido verificado exitosamente');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($zone, $message)
    {

        $zone = Zone::where('zone_id', $zone)->firstOrFail();

        $message = Message::where('message_id', $message)->firstOrFail();

        $message->delete();

        return redirect()
            ->route('messages', $zone->zone_id)
            ->with('msg', 'El mensaje ha sido eliminado correctamente');

    }

    /**
     */
    public function create_comment_for_message(Request $request, $zone_id, $message_id)
    {
        $validate_msgs = [

            'message.required' => 'El comentario es requerido',

        ];

        $validate = Validator::make($request->all(), [

            'message' => 'required|string',

        ], $validate_msgs)->validate();

        $user = auth()->user();

        $comment              = new Comment();
        $comment->user_id     = $user->id;
        $comment->message_id  = $message_id;
        $comment->description = $request->message;
        $comment->save();

        $message = Message::where('message_id', $message_id)->first();

        $members = $message->members();

        $notification_data = [
            'type'    => 'Nuevo Comentario',
            'subject' => 'Nuevo Comentario',
            'comment' => str_limit($request->message, 50),
            'author'  => $user,
            'message' => $message,
        ];

        Notification::send($members, new NewComment($notification_data));

        if ($request->ajax()) {

            $response = [
                'description' => $comment->description,
                'author'      => $comment->author->name,
                'date'        => $comment->created_at->diffForHumans(),
            ];

            return response()->json($response);

        } else {

            return redirect()->route('show-message', ['zone' => $zone_id, 'message' => $message_id]);

        }

    }

    public function validation(Request $request)
    {
        $rules = [
            'subject'     => 'required|max:200|string',
            'category_id' => 'required|exists:categories,category_id',
            'severity_id' => 'required|exists:severities,id',
            'description' => 'nullable',
        ];

        $msgs = [
            'subject.required'     => 'El campo asunto es requerido',
            'subject.max'          => 'El campo asunto excede 200 carácteres',
            'subject.string'       => 'El campo asunto solo acepta carácteres alfanúmericos',
            'category_id.required' => 'El campo categoría es requerido',
            'category_id.exists'   => 'El valor de categoría no existe en nuestra base de datos',
            'severity_id.required' => 'El campo severidad es requerido',
            'severity_id.exists'   => 'El valor de severidad no existe en nuestra base de datos',
        ];

        $validate = Validator::make($request->all(), $rules, $msgs)->validate();
    }
}
