<?php

namespace App\Http\Controllers\Zones;

use App\Http\Controllers\Controller;
use App\Models\Zones\Task;
use App\Models\Zones\Zone;
use Illuminate\Http\Request;
use App\Notifications\NewTask;
use Notification;


class TasksController extends Controller
{


    public function __construct(){


        


    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($zone)
    {
        $zone = Zone::where('zone_id', $zone)->firstOrFail();

        $tasks = $zone->tasks()->latest()->paginate(12);

        $checkin  = null;
        
        if ( auth()->user()->role == 'agent' ) {

            $agent = auth()->user()->agent;

            $checkin = app('App\Http\Controllers\CheckInController')->find_current_check_in( $agent, $zone );

        }

        return view('zones.tasks.index', compact('zone', 'tasks', 'checkin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $zone)
    {

        $zone = Zone::where('zone_id', $zone)->first();

        $task = new Task();

        $task->task_id     = 'task-' . uniqid();
        $task->zone_id     = $zone->zone_id;
        $task->description = $request->description;
        $task->priority    = $request->priority;

        $task->save();


        $notification_data = [

            'notification' => 'Se ha agregado una nueva tarea en la zona ' . $zone->name,
            'link' => route('tasks', $zone->zone_id)

        ];


        foreach ($zone->agents as $key => $assigned) {
            

            $assigned->agent->user->notify(new NewTask($notification_data));


        }


        return redirect()
            ->route('tasks', $zone->zone_id)
            ->with('msg', 'Se ha creado una nueva tarea');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($zone, $task)
    {

        $task = Task::where('task_id', $task)->firstOrFail();

        $task->delete();

        return redirect()
            ->back()
            ->with('msg', 'La tarea ha sido eliminada correctamente');

    }
}
