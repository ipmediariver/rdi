<?php

namespace App\Http\Controllers\Zones;

use App\Http\Controllers\Controller;
use App\Models\Zones\Zone;
use App\Models\Users\Contact;
use App\Models\Zones\ZoneCategoryContact;
use Illuminate\Http\Request;
use Validator;

class ZoneCategoryContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($zone, $contact)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($zone)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $zone, $contact)
    {
        $zone    = Zone::where( 'zone_id', $zone )->firstOrFail();
        $contact = Contact::where( 'contact_id', $contact )->firstOrFail();

        $contact->categories_contact()->delete(); 

        if(isset($request->categories)){

            foreach($request->categories as $category){

                $zone_category_contact = new ZoneCategoryContact();

                $zone_category_contact->zone_id     = $zone->zone_id;
                $zone_category_contact->category_id = $category;
                $zone_category_contact->contact_id  = $contact->contact_id;

                $zone_category_contact->save();

            }
            
        }


        return redirect()
            ->back()
            ->with('msg', 'El contacto ha sido asignado a las categorias seleccionadas');       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ZoneCategoryContact  $zone_category_contact
     * @return \Illuminate\Http\Response
     */
    public function show($zone, $zone_category_contact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ZoneCategoryContact  $zone_category_contact
     * @return \Illuminate\Http\Response
     */
    public function edit($zone, $zone_category_contact)
    {
        $zone                  = Zone::where('zone_id', $zone)->firstOrFail();
        $zone_category_contact = ZoneCategoryContact::where('id', $zone_category_contact)->firstOrFail();

        return view('zones.category_contacts.edit', compact('zone', 'zone_category_contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ZoneCategoryContact  $zone_category_contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $zone, $zone_category_contact)
    {
        $zone                  = Zone::where('zone_id', $zone)->firstOrFail();
        $zone_category_contact = ZoneCategoryContact::where('id', $zone_category_contact)->firstOrFail();

        $this->validate_zone_category_contact($request);

        $zone_category_contact->category_id = $request->category_id;
        $zone_category_contact->email       = $request->email;

        $zone_category_contact->save();

        return redirect()
            ->route('zones.category-contacts.index', $zone->zone_id)
            ->with('msg', 'El contacto ha sido actualizado y asignado a la categoria correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ZoneCategoryContact  $zone_category_contact
     * @return \Illuminate\Http\Response
     */
    public function destroy($zone, $zone_category_contact)
    {
        $zone                  = Zone::where('zone_id', $zone)->firstOrFail();
        $zone_category_contact = ZoneCategoryContact::where('id', $zone_category_contact)->firstOrFail();

        $zone_category_contact->delete();

        return redirect()
            ->route('zones.category-contacts.index', $zone->zone_id)
            ->with('msg', 'El contacto ha sido eliminado de la categoria correctamente');
    }

    public function validate_zone_category_contact($request)
    {
        $validate_cond = [
            'category_id' => 'required|exists:categories,category_id',
        ];

        $validate_msgs = [
            'category_id.required' => 'El campo Categoría es requerido',
            'category_id.exists'   => 'El campo Categoría no existe en nuestra base de datos',
        ];

        $validate = Validator::make($request->all(), $validate_cond, $validate_msgs)->validate();
    }
}
