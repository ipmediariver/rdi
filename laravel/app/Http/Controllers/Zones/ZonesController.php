<?php

namespace App\Http\Controllers\Zones;

use App\Http\Controllers\Controller;
use App\Models\Zones\Zone;
use App\Models\Zones\Category;
use Illuminate\Http\Request;

class ZonesController extends Controller
{

    public function __construct()
    {

        $this->middleware('admin', ['except' => ['index', 'show']]);
        $this->middleware('assigned_zone', ['except' => ['index']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = auth()->user();

        $zones = null;

        if ($user->role == 'admin') {

            $zones = Zone::latest()->get();

        } elseif ($user->role == 'agent') {

            $agent = $user->agent;

            $zones = $agent->zones();

        } elseif ($user->role == 'contact') {

            $contact = $user->contact;
            $zones   = $contact->zones();

        }

        return view('zones.index', compact('zones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('zones.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $zone          = new Zone($request->except('_token'));
        $zone->zone_id = 'zone-' . uniqid();

        $zone->save();

        return redirect()
            ->route('show-zone', $zone->zone_id)
            ->with('msg', 'Se ha creado una nueva zona correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $user = auth()->user();

        $zone = Zone::where('zone_id', $id)->firstOrFail();

        $categories = Category::get()->sortBy('fullName');

        if ($user->role == 'admin') {

            return view('zones.show', compact('zone', 'categories'));

        } elseif ($user->role == 'agent') {

            return redirect()->route('messages', $zone->zone_id);

        } elseif ($user->role == 'contact') {

            return view('zones.show', compact('zone', 'categories'));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $zone = Zone::where('zone_id', $id)->firstOrFail();

        return view('zones.edit', compact('zone'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $zone = Zone::where('zone_id', $id)->firstOrFail();

        $zone->update($request->except('_token'));

        return redirect()
            ->route('show-zone', $zone->zone_id)
            ->with('msg', 'La zona ha sido actualizada correctamente');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $zone = Zone::where('zone_id', $id)
            ->firstOrFail();

        $zone->agents()->delete();
        $zone->contacts()->delete();
        $zone->messages()->delete();
        $zone->tasks()->delete();
        $zone->guests()->delete();

        $zone->delete();

        return redirect()
            ->route('zones')
            ->with('msg', 'La zona ha sido eliminada correctamente');
    }
}
