<?php

namespace App\Http\Middleware;

use Closure;

use \App\Models\Zones\Zone;

class AssignedZoneMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        $user = auth()->user();

        if($user->role == 'agent'){

            $zone = $user->agent->assignedZones()->where('zone_id', request()->zone)->first();

            if($zone){

                return $next($request);

            }else{

                return redirect()->route('dashboard');

            }


        }

        return $next($request);

    }
}
