<?php

namespace App\Http\Middleware;

use Closure;

class ContactMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();

        if( $user->role == 'contact' ){

            if ( $user->contact->assignedZones ) {

                return $next( $request );

            } else {

                return redirect()->route('dashboard');

            }
             
        } else {

            return redirect()->route('dashboard');

        }
    }
}
