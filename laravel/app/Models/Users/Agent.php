<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    protected $table   = 'agents';
    public $timestamps = true;
    protected $guarded = [];


    
    public function user(){


    	return $this->belongsTo('App\Models\Users\User', 'user_id', 'id');


    }

    public function assignedZones(){


    	return $this->hasMany('App\Models\Zones\ZoneAgent', 'agent_id', 'agent_id');


    }

    public function zones(){


    	$assigned_zones = $this->assignedZones()->latest()->get();

    	$zones = [];

    	foreach ($assigned_zones as $assigned) {
    	
    		array_push($zones, $assigned->zone);

    	}

    	return $zones;


    }


    public function check_in(){

        return $this->hasOne('App\Models\Users\CheckIn', 'agent_id', 'agent_id');

    }


}
