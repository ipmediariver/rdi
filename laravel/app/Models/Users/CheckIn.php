<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class CheckIn extends Model
{
    protected $table   = 'check_ins';
    public $timestamps = true;
    protected $guarded = [];

    public function agent(){


        return $this->hasOne('App\Models\Users\Agent', 'agent_id', 'agent_id');


    }


    public function zone(){


        return $this->hasOne('App\Models\Zones\Zone', 'zone_id', 'zone_id');


    }
}
