<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table   = 'contacts';
    public $timestamps = true;
    protected $guarded = [];


    public function user(){


    	return $this->belongsTo('App\Models\Users\User', 'user_id', 'id');


    }

    public function assignedZones(){


    	return $this->hasMany('App\Models\Zones\ZoneContact', 'contact_id', 'contact_id');


    }

    public function categories_contact()
    {
        return $this->hasMany('App\Models\Zones\ZoneCategoryContact', 'contact_id', 'contact_id');
    }

    public function zones(){


        $assigned_zones = $this->assignedZones()->latest()->get();

        $zones = [];

        foreach ($assigned_zones as $assigned) {
        
            array_push($zones, $assigned->zone);

        }

        return $zones;


    }

}
