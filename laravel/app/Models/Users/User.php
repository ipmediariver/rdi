<?php

namespace App\Models\Users;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role', 'username'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
        'password', 'remember_token',
        
    ];

    public function agent(){

        return $this->hasOne('App\Models\Users\Agent', 'user_id', 'id');

    }

    public function contact(){

        return $this->hasOne('App\Models\Users\Contact', 'user_id', 'id');

    }

    public function user_comment(){

        return $this->hasOne('App\Models\Users\Comment', 'user_id', 'id');

    }

    public function get_dashboard(){

        return 'Dashboard';

    }
}
