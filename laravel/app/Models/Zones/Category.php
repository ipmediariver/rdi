<?php

namespace App\Models\Zones;

use Illuminate\Database\Eloquent\Model;

use App\Models\Zones\Zone;

class Category extends Model
{
    protected $table   = 'categories';
    public $timestamps = true;
    protected $guarded = [];


    public function messages()
    {
    	return $this->hasMany('App\Models\Zones\Message', 'category_id', 'category_id');
    }

    public function category_contacts()
    {
        return $this->hasMany('App\Models\Zones\ZoneCategoryContact', 'category_id', 'category_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Zones\Category', 'parent_id', 'id');
    }

    public function childs()
    {
        return $this->hasMany('App\Models\Zones\Category', 'parent_id', 'id');
    }

    public function getMasterParentAttribute()
    {
        return $this->all_parents()->last();
    }

    public function all_parents()
    {
        $parents = collect([$this]);

        $parent = $this->parent;

        while (!is_null($parent)) {
            $parents->push($parent);
            $parent = $parent->parent;
        }

        return $parents;
    }

    public function getFullNameAttribute(){

        $t = $this;

        $name = [];

        foreach($t->all_parents() as $parent){


            array_push($name, $parent->name); 

        }

        $name = array_reverse($name);
        $name = join(' | ', $name);

        return $name;

    }

    public function all_childs()
    {
        $all_childs = collect([$this]);
        $next_childs = collect($this->childs);
        
        while (!is_null($next_childs)) 
        {

            $add_to_next_childs = collect([]);

            $next_childs->each(function ($child) use ($all_childs, $add_to_next_childs) {
                $all_childs->push($child);
                $child->childs()->each(function ($c) use ($add_to_next_childs) {
                    $add_to_next_childs->push($c);
                });
            });

            if ($add_to_next_childs->count() > 0) {
                $next_childs = $add_to_next_childs;
            } else {
                $next_childs = null;
            }
        }

        return $all_childs;
    }


    public function percent($zone){

        $zone = Zone::where('zone_id', $zone)->first();

        $total_zone_msgs = $zone->messages->count();

        $total_msgs_from_this_categories_in_zone = $this->messages()->where('zone_id', $zone->zone_id)->count();

        if($total_zone_msgs > 0){

            $percent = ($total_msgs_from_this_categories_in_zone * 100) / $total_zone_msgs;
            
        }else{

            $percent = 0;

        }


        $percent = number_format($percent, 2);

        return $percent;

    }
}
