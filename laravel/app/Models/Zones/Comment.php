<?php

namespace App\Models\Zones;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public $time_of_comment = null;

    public function author(){

        return $this->belongsTo('App\Models\Users\User', 'user_id', 'id');

    }


    public function message(){


        return $this->belongsTo('App\Models\Zones\Message', 'message_id', 'message_id');


    }
}
