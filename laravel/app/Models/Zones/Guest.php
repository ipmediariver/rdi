<?php

namespace App\Models\Zones;

use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
    protected $table   = 'guests';
    public $timestamps = true;
    protected $guarded = [];


    public function zone(){

    	return $this->belongsTo('App\Models\Security\Zones', 'zone_id', 'zone_id');

    }

    public function department(){

        return $this->hasOne('App\Models\Zones\Department', 'department_id', 'department_id');
    }

    public static function diff_guest_time( $str_start_time, $str_end_time, $number_format = false ){
        $time_elapsed = null;

        if ( $str_start_time ) {
            
            $start_time = \Carbon\Carbon::parse( $str_start_time );
            
            if ( $number_format ) {
                $time_elapsed = 0; 

                if ( $str_end_time ) {
                
                    $end_time     = \Carbon\Carbon::parse( $str_end_time );
                    $time_elapsed = ($start_time->diffInMinutes( $end_time ) / 60); 
                    $time_elapsed = round( $time_elapsed, 2 );
                
                }
     
            } else {
                
                if ( $str_end_time ) {
     
                    $end_time     = \Carbon\Carbon::parse( $str_end_time );
                    $time_elapsed = date( 'H \h\r\s i \m\i\n', mktime( 0, $start_time->diffInMinutes( $end_time ) ) ); 
                    $time_elapsed = str_replace('00 hrs ', '', $time_elapsed );
                    
                }            
     
            }

        }


        return $time_elapsed;
    }
}
