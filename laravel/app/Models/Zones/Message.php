<?php

namespace App\Models\Zones;

use Illuminate\Database\Eloquent\Model;

use \App\Models\Users\User;

class Message extends Model
{
    protected $table   = 'messages';
    public $timestamps = true;
    protected $guarded = [];

    public function author()
    {

        return $this->belongsTo('App\Models\Users\Agent', 'agent_id', 'agent_id');

    }

    public function category()
    {

        return $this->belongsTo('App\Models\Zones\Category', 'category_id', 'category_id');

    }

    public function comments()
    {

        return $this->hasMany('App\Models\Zones\Comment', 'message_id', 'message_id');

    }

    public function zone()
    {

        return $this->belongsTo('App\Models\Zones\Zone', 'zone_id', 'zone_id');

    }


    public function get_severity(){


        return $this->belongsTo('App\Models\Zones\Severity', 'severity_id', 'id');


    }


    public function getSeverityAttribute(){



        $severity = $this->get_severity;

        if($severity){


            return $severity;


        }else{


            $find_medium_severity = \App\Models\Zones\Severity::where('name', 'Media')->first();


            return $find_medium_severity;


        }


    }


    public function ajax_comments()
    {

        $comments = $this->comments;

        $ajax_comments = [];

        foreach ($comments as $key => $comment) {

            $comment = [

                'description' => $comment->description,
                'author'      => $comment->author ? $comment->author->name : 'Admin',
                'date'        => $comment->created_at->diffForHumans(),

            ];

            array_push($ajax_comments, $comment);

        }

        return $ajax_comments;

    }

    public function members()
    {

        $user    = auth()->user();
        $members = [];

        $comments = $this->comments()->where('user_id', '<>', $user->id)->get();
        $admins    = User::where('role', 'admin')->where('id', '<>', $user->id)->get();

        foreach ($comments as $comment) {

            $author = $comment->author;

            if (!in_array($author, $members)) {

                array_push($members, $author);

            }

        }

        foreach ($admins as $admin) {

            if (!in_array($admin, $members)) {

                array_push($members, $admin);

            }
        }

        return collect($members);

    }

}
