<?php

namespace App\Models\Zones;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    protected $table   = 'zones';
    public $timestamps = true;
    protected $guarded = [];


    public function agents(){


    	return $this->hasMany('App\Models\Zones\ZoneAgent', 'zone_id', 'zone_id');

    }

    public function contacts(){


    	return $this->hasMany('App\Models\Zones\ZoneContact', 'zone_id', 'zone_id');

    }

    public function messages(){


    	return $this->hasMany('App\Models\Zones\Message', 'zone_id', 'zone_id');


    }

    public function tasks(){


    	return $this->hasMany('App\Models\Zones\Task', 'zone_id', 'zone_id');


    }

    public function guests(){


    	return $this->hasMany('App\Models\Zones\Guest', 'zone_id', 'zone_id');


    }

    public function checkIns(){


        return $this->hasMany('App\Models\Users\CheckIn', 'zone_id', 'zone_id');


    }

    public function category_contacts(){


        return $this->hasMany('App\Models\Zones\ZoneCategoryContact', 'zone_id', 'zone_id');


    }
}
