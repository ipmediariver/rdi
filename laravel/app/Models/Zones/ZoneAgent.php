<?php

namespace App\Models\Zones;

use Illuminate\Database\Eloquent\Model;

class ZoneAgent extends Model
{
    protected $table   = 'zone_agents';
    public $timestamps = true;
    protected $guarded = [];


    public function agent(){


    	return $this->belongsTo('App\Models\Users\Agent', 'agent_id', 'agent_id');


    }

    public function zone(){


    	return $this->belongsTo('App\Models\Zones\Zone', 'zone_id', 'zone_id');


    }

}
