<?php

namespace App\Models\Zones;

use Illuminate\Database\Eloquent\Model;

class ZoneCategoryContact extends Model
{
    public function contact()
    {
        return $this->belongsTo('App\Models\Users\Contact', 'contact_id', 'contact_id');
    }

    public function category()
    {

        return $this->belongsTo('App\Models\Zones\Category', 'category_id', 'category_id');

    }

    public function zone()
    {
        return $this->belongsTo('App\Models\Zones\Zone', 'zone_id', 'zone_id');
    }
}
