<?php

namespace App\Models\Zones;

use Illuminate\Database\Eloquent\Model;

class ZoneContact extends Model
{
    
	protected $table   = 'zone_contacts';
	public $timestamps = true;
	protected $guarded = [];


	public function contact(){


		return $this->belongsTo('App\Models\Users\Contact', 'contact_id', 'contact_id');


	}

    public function zone(){


        return $this->belongsTo('App\Models\Zones\Zone', 'zone_id', 'zone_id');


    }

}
