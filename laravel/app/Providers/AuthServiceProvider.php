<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        
        \Blade::directive('role', function ($expression) {

            $expression = str_replace("'", '', $expression);
            $expression = str_replace('"', '', $expression);

            if(\Auth::user()){
                if($expression == 'admin'){
                    return "<?php if(Auth::user()->role == 'admin'): ?>";
                }elseif($expression == 'agent'){
                    return "<?php if(Auth::user()->role == 'agent'): ?>";
                }elseif($expression == 'contact'){
                    return "<?php if(Auth::user()->role == 'contact'): ?>";
                }
            }
        });

        \Blade::directive('endrole', function ($expression) {
            if(\Auth::user()){
                return '<?php endif; ?>';
            }
        });

    }
}
