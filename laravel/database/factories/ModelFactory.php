<?php

use App\Models\Users\Agent;
use App\Models\Zones\Department;
use App\Models\Users\Contact;
use App\Models\Users\User;
use App\Models\Zones\Category;
use App\Models\Zones\Guest;
use App\Models\Zones\Message;
use App\Models\Zones\Task;
use App\Models\Zones\Zone;
use App\Models\Zones\ZoneAgent;
use App\Models\Zones\ZoneContact;
use App\Models\Zones\Severity;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
 */

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name'           => $faker->name,
        'email'          => $faker->unique()->safeEmail,
        'username'       => $faker->username,
        'password'       => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(Department::class, function (Faker\Generator $faker) {
    return [
        'department_id' => 'department-' . uniqid(),
        'name'          => $faker->company,
        'description'   => $faker->realText(150),
    ];
});

$factory->define(Guest::class, function (Faker\Generator $faker) {
    return [
        'guest_id'    => 'guest-' . uniqid(),
        'zone_id'     => function () {

            $rand_id = rand(1, Zone::get()->count());

            $zone = Zone::where('id', $rand_id)->first();

            return $zone->zone_id;

        },
        'department_id'     => function () {

            $rand_id = rand(1, Department::get()->count());

            $department = Department::where('id', $rand_id)->first();

            return $department->department_id;

        },
        'name'        => $faker->name,
        'gender'      => $faker->randomElement($array = array('m', 'f')),
        'subject'     => $faker->realText(150),
        'host'        => $faker->name,
        'description' => $faker->realText(150),
        'start_time'  => \Carbon\Carbon::now()->subDays(5)->addDays(rand(1, 5)),
        'id_type'     => $faker->randomElement($array = array('id', 'license', 'passpord', 'other')),
        'id_number'   => 'ASDF65465S4FD',
    ];
});

$factory->define(Agent::class, function (Faker\Generator $faker) {
    return [
        'user_id'  => function () {

            return factory('App\Models\Users\User')->create([
                'role' => 'agent',
            ])->id;

        },
        'agent_id' => 'agent-' . uniqid(),
    ];
});

$factory->define(Contact::class, function (Faker\Generator $faker) {
    return [
        'user_id'    => function () {

            return factory('App\Models\Users\User')->create([
                'role' => 'contact',
            ])->id;

        },
        'contact_id' => 'contact-' . uniqid(),
    ];
});

$factory->define(Zone::class, function (Faker\Generator $faker) {
    return [
        'zone_id' => 'zone-' . uniqid(),
        'name'    => $faker->company,
        'address' => $faker->address,
    ];
});

$factory->define(Category::class, function (Faker\Generator $faker) {
    return [
        'category_id' => 'category-' . uniqid(),
        'name'        => $faker->word,
        'color'       => $faker->hexcolor,
    ];
});

$factory->define(ZoneAgent::class, function (Faker\Generator $faker) {
    return [
        'zone_id'  => function () {

            $rand_id = rand(1, Zone::get()->count());

            $zone = Zone::where('id', $rand_id)->first();

            return $zone->zone_id;

        },
        'agent_id' => function () {

            $rand_id = rand(1, Agent::get()->count());

            $agent = Agent::where('id', $rand_id)->first();

            return $agent->agent_id;

        },
    ];
});

$factory->define(ZoneContact::class, function (Faker\Generator $faker) {
    return [
        'zone_id'    => function () {

            $rand_id = rand(1, Zone::get()->count());

            $zone = Zone::where('id', $rand_id)->first();

            return $zone->zone_id;

        },
        'contact_id' => function () {

            $rand_id = rand(1, Contact::get()->count());

            $contact = Contact::where('id', $rand_id)->first();

            return $contact->contact_id;

        },
    ];
});


$factory->define(Severity::class, function(Faker\Generator $faker){

    return [

        'name' => 'Media',
        'color' => '#44a23e'

    ];

});


$factory->define(Message::class, function (Faker\Generator $faker) {

    $images = [

        'https://via.placeholder.com/300x' . rand(300, 360),
        'https://via.placeholder.com/300x' . rand(300, 360),
        'https://via.placeholder.com/300x' . rand(300, 360),

    ];

    $images = json_encode($images);

    return [
        'message_id'  => 'msg-' . uniqid(),
        'subject'     => $faker->realText(70),     
        'description' => $faker->realText(150),
        'category_id' => function () {

            $rand_id = rand(1, Category::get()->count());

            $category = Category::where('id', $rand_id)->first();

            return $category->category_id;

        },
        'images'      => $images,
        'agent_id'    => function () {

            $rand_id = rand(1, Agent::get()->count());

            $agent = Agent::where('id', $rand_id)->first();

            return $agent->agent_id;

        },
        'zone_id'     => function () {

            $rand_id = rand(1, Zone::get()->count());

            $zone = Zone::where('id', $rand_id)->first();

            return $zone->zone_id;

        },
        'verified' => rand(1,0),
        'created_at'  => \Carbon\Carbon::now()->subDays(7)->addDays(rand(1, 7)),
    ];
});

$factory->define(Task::class, function (Faker\Generator $faker) {
    return [
        'task_id'     => 'task-' . uniqid(),
        'zone_id'     => function () {

            $rand_id = rand(1, Zone::get()->count());

            $zone = Zone::where('id', $rand_id)->first();

            return $zone->zone_id;

        },
        'description' => $faker->realText(150),
        'priority'    => $faker->randomElement($array = array('l', 'n', 'h')),
    ];
});

