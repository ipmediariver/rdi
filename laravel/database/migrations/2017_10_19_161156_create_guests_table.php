<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGuestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('guest_id');
            $table->string('zone_id');
            $table->string('department_id');
            $table->string('name');
            $table->string('gender');
            $table->text('subject');
            $table->string('host')->nullable();
            $table->text('description')->nullable();
            $table->string('start_time');
            $table->string('end_time')->nullable();
            $table->string('id_type');
            $table->string('id_number');
            $table->text('finger_print')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guests');
    }
}
