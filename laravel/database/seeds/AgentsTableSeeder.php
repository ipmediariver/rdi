<?php

use Illuminate\Database\Seeder;

use App\Models\Users\Agent;
use App\Models\Users\User;

class AgentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Agent::class, 20)->create();

        // factory(Agent::class, 1)->create();
    }
}
