<?php

use Illuminate\Database\Seeder;

use App\Models\Zones\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	factory(Category::class, 1)->create([
    		'name' => 'Control de accesos'
    	]);
    	factory(Category::class, 1)->create([
    		'name' => 'Instalación'
    	]);
    	factory(Category::class, 1)->create([
    		'name' => 'Mantenimiento'
    	]);
    	factory(Category::class, 1)->create([
    		'name' => 'Otros'
    	]);
    	factory(Category::class, 1)->create([
    		'name' => 'Seguridad'
    	]);

    }
}
