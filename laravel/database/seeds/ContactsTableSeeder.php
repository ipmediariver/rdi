<?php

use Illuminate\Database\Seeder;

use App\Models\Users\Contact;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        

    	factory(Contact::class, 10)->create();

    	// factory(Contact::class, 1)->create();


    }
}
