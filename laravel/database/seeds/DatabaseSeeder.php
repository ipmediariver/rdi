<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategoriesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(DepartmentTableSeeder::class);
        $this->call(ZonesTableSeeder::class);
        $this->call(GuestsTableSeeder::class);
        $this->call(AgentsTableSeeder::class);
        $this->call(ContactsTableSeeder::class);
        $this->call(ZoneAgentsTableSeeder::class);
        $this->call(ZoneContactsTableSeeder::class);
        $this->call(MessagesTableSeeder::class);
        $this->call(TasksTableSeeder::class);
        $this->call(SeveritiesTableSeeder::class);
    }
}
