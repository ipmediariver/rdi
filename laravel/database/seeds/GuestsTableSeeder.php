<?php

use Illuminate\Database\Seeder;

use App\Models\Zones\Guest;

class GuestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Guest::class, 40)->create();
    }
}
