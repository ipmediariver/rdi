<?php

use Illuminate\Database\Seeder;

use App\Models\Zones\Severity;

class SeveritiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        

    	factory(Severity::class, 1)->create([


    		'name' => 'Baja',
    		'color' => '#3ac1c7'


    	]);


    	factory(Severity::class, 1)->create([


    		'name' => 'Media',
    		'color' => '#cec129'


    	]);


    	factory(Severity::class, 1)->create([


    		'name' => 'Alta',
    		'color' => '#ce2929'


    	]);


    }
}
