<?php

use Illuminate\Database\Seeder;

use App\Models\Zones\Task;


class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Task::class, 50)->create();
    }
}
