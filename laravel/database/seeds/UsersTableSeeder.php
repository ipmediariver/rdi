<?php

use App\Models\Users\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        factory(User::class, 1)->create([

            'name'     => 'Administrator',
            'email'    => 'admin@admin.com',
            'username' => 'admin'

        ]);

    }
}
