<?php

use Illuminate\Database\Seeder;

use App\Models\Zones\ZoneAgent;

class ZoneAgentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(ZoneAgent::class, 15)->create();
    }
}
