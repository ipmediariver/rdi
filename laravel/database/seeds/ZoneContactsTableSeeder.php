<?php

use Illuminate\Database\Seeder;

use App\Models\Zones\ZoneContact;

class ZoneContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        

    	factory(ZoneContact::class, 15)->create();


    }
}
