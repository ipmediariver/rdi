<?php

use Illuminate\Database\Seeder;

use App\Models\Zones\Zone;

class ZonesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Zone::class, 8)->create();
    }
}
