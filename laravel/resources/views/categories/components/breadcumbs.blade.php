@if ( !empty($category->parent_id) )
    <ol class="breadcrumb mb-3">
        @foreach($category->all_parents()->reverse() as $childs)
            @if(!$loop->last)
                <li>
                    <a href="{{ route('show-category', $childs->category_id) }}">
                        {{ $childs->name }}
                    </a>
                </li>
            @else
                <li class="active">
                    {{ $childs->name }}
                </li>
            @endif
        @endforeach
    </ol>
@endif
