@extends('layouts.app')

@section('content')

	<div class="row">
		<div class="col-sm-4 col-sm-offset-4">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Nueva Categoría</h3>
				</div>
				<div class="panel-body">
					
					<form action="{{ route('store-category') }}{{ (isset($_GET['parent']))?'?parent='.$_GET['parent']:'' }}" method="POST">
						@include('categories.partials.form')
						<button type="submit" class="btn btn-primary">Guardar Categoría</button>
					</form>

				</div>
			</div>
		</div>
	</div>

@stop