@extends('layouts.app')

@section('content')

	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Editar Categoría</h3>
				</div>
				<div class="panel-body">
					
					<form action="{{ route('update-category', $category->category_id) }}" method="POST">
						{{ method_field('PATCH') }}
						@include('categories.partials.form')
						<button type="submit" class="btn btn-primary">Guardar Cambios</button>
						<button type="submit" class="btn btn-danger" form="deleteCategoryForm" onclick="return confirm('¿Estas seguro que deseas eliminar esta categoría?')">
							Eliminar Categoría
						</button>
					</form>

					<form action="{{ route('delete-category', $category->category_id) }}" id="deleteCategoryForm" method="POST">
						{{ method_field('DELETE') }}
						{{ csrf_field() }}
					</form>

				</div>
			</div>
		</div>
	</div>

@stop