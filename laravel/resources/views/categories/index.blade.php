@extends('layouts.app')

@section('content')

	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading d-flex align-items-center d-flex-mobile">
					<h3 class="panel-title">
						Categorías
					</h3>
					<a href="{{ route('new-category') }}/" class="btn btn-primary btn-sm ml-auto">Nueva</a>
				</div>
				@if(count($categories) > 0)
					<div class="table-responsive">
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th colspan="2">Nombre</th>
									<th class="text-center">Sub categorías</th>
									<th width="10%" class="text-center">Ver</th>
								</tr>
							</thead>
							<tbody>
								@foreach($categories as $category)
						
								<tr>
									<td style="padding: 0; width: 10px; background-color: {{ $category->color }} "></td>
									<td>{{ $category->name }}</td>
									<td class="text-center">{{ $category->childs()->count() }}</td>
									<td class="text-center"><a href="{{ route('show-category', $category->category_id) }}/">Ver</a></td>
								</tr>

								@endforeach
							</tbody>
						</table>
					</div>
				@else
					<div class="panel-body">
						<p class="m-0">No se han agregado categorías</p>
					</div>
				@endif
			</div>
		</div>
	</div>


@stop
