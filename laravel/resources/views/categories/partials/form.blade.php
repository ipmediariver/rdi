{{ csrf_field() }}
<div class="form-group">
	<label for="name">Nombre de la categoría</label>
	<input type="text" class="form-control" value="{{ $category->name or old('name') }}" name="name">
</div>
<div class="form-group">
	<label for="name">Color</label>
	<input type="color" name="color" value="{{ $category->color or old('color') }}">
</div>