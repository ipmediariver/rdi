@extends('layouts.app')

@section('content')

	
	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			@include('categories.components.breadcumbs')
			<div class="panel panel-default mb-3">
				<div class="panel-heading d-flex align-items-center">
					<h3 class="panel-title">Información de categoría</h3>
					<a href="{{ route('edit-category', $category->category_id) }}" class="btn btn-sm btn-primary ml-auto">
						Editar
					</a>
				</div>
				<table class="table table-bordered table-striped">
					<tr>
						<td style="padding: 0; width: 10px; background-color: {{ $category->color }}"></td>
						<td>{{ $category->name }}</td>
					</tr>
				</table>
			</div>
			<div class="panel panel-default mb-3">
				<div class="panel-heading d-flex align-items-center">
					<h3 class="panel-title">Subcategoría</h3>
					<a href="{{ route('new-category') }}?parent={{ $category->id }}" class="btn btn-sm btn-primary ml-auto">
						Agregar
					</a>
				</div>
				@if ($category->childs()->count() > 0)
					<table class="table table-bordered table-striped">
						@foreach ($category->childs as $category_child)
							<tr>
								<td style="padding: 0; width: 10px; background-color: {{ $category_child->color }} "></td>
								<td>{{ $category_child->name }}</td>
								<td class="text-center">{{ $category_child->childs()->count() }} Sub categoría(s)</td>
								<td class="text-center"><a href="{{ route('show-category', $category_child->category_id) }}/">Ver</a></td>
							</tr>
						@endforeach
					</table>
				@else
					<div class="panel-body">
						No existen Subcategorías de {{ $category->name }}
					</div>
				@endif
			</div>
		</div>
	</div>
	

@stop