<div class="col-sm-4 col-xs-12" style="margin-bottom: 8px;">
	<div class="panel panel-default m-0" style="height: 100%">

		@if($message->verified)
	
			<div class="verified_icon">
				<i class="fa fa-check"></i>
			</div>

		@endif
		
		@php 
			
			$imgs = json_decode($message->images);


			$imgs_tot = $imgs ? count($imgs) : 0;

		@endphp

		@if($imgs_tot > 0)
			
			<div class="panel-owl">
				
				<div class="owl-carousel">

					@foreach($imgs as $img)
						<a href="{{ route('show-message', ['zone' => $zone->zone_id, 'message' => $message->message_id]) }}/">
							@php
								$have_url = strrpos($img, "http");
								if ($have_url === false) {
									$img = url('/').'/uploads7e8r2t58d/'.$img;
								}
								$img = str_replace('https://v2-rdi.proteusconsulting.com/', 'http://v2.rdimx.com/', $img);
							@endphp
							<div class="panel-thumb" style="background-image: url({{$img}})"></div>

						</a>
					@endforeach

				</div>
				
			</div>


		@endif

		<div class="panel-body">
			<div class="d-flex" style="margin-bottom: 10px;">
				<div style="margin-right: 7px">
					@if($imgs_tot > 0)
						<span style="margin-right: 5px;">
							<span class="badge badge_icon">{{ $imgs_tot }} <i class="fa fa-camera"></span></i>
						</span>
					@endif
					@if(count($message->comments) > 0)
						<span class="badge badge_icon">{{ count($message->comments) }} <i class="fa fa-comment"></i></span>
					@endif
				</div>
			</div>
			<p class="m-0">
				<small>
					{{ $message->created_at->format('d M, Y - h:i a') }}
				</small>
			</p>
			<p class="m-0">

				<i class="fa fa-circle" style="color: {{ $message->severity->color }}"></i>
			
				Severidad {{ $message->severity->name }}

			</p>
			<p class="m-0 text-uppercase" style="display: inline-block; font-weight: bold; color: {{ $message->category ? $message->category->color : '#222'  }}">

				<small>{{ $message->category->fullName or 'Sin categoría' }}</small>

			</p>
			<a href="{{ route('show-message', ['zone' => $zone->zone_id, 'message' => $message->message_id]) }}/" class="m-0" style="display: block;">
				{{ $message->subject }}
			</a>
			<p class="text-muted"><small>{{ str_limit($message->description, 80) }}</small></p>
		</div>
	</div>
</div>