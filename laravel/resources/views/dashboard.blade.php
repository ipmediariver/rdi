@extends('layouts.app')

@section('content')

<div id="charts">
    
    @if(count($zones) > 0)

        <div class="row">
            @foreach($zones as $zone)
                <div class="col-sm-3">
                    <div class="panel panel-default">
                        
                        <div class="panel-heading">
                            <h3 class="panel-title pull-left">
                                {{ str_limit($zone->name, 20) }}
                            </h3>
                            @role('agent')
                                @php
                                    $check = $zone->checkIns()->where('agent_id', auth()->user()->agent->agent_id )->where('check_out', null)->first();
                                @endphp

                                @if ($check)
                                    <i class="fa fa-power-off active_check_in"></i>
                                @endif
                            @endrole
                        </div>
                        
                        <ul class="list-group">
                            <li class="list-group-item">
                                Mensajes
                                <span class="pull-right badge">
                                    {{ count($zone->messages) }}
                                </span>
                            </li>

                            <li class="list-group-item">
                                Agentes
                                <span class="pull-right badge">
                                    {{ count($zone->agents) }}
                                </span>
                            </li>

                            <li class="list-group-item">
                                Contactos
                                <span class="pull-right badge">
                                    {{ count($zone->contacts) }}
                                </span>
                            </li>

                            {{-- <li class="list-group-item">
                                Visitas
                                <span class="pull-right badge">
                                    {{ count($zone->guests) }}
                                </span>
                            </li> --}}
                        </ul>

                        <div class="panel-footer text-center">
                            <a href="{{ route('show-zone', $zone->zone_id) }}/">
                                Ver Zona <i class="fa fa-angle-right fa-fw"></i>
                            </a>
                        </div>

                    </div>
                </div>
            @endforeach
        </div>

    @else

        <div class="panel panel-default">
            <div class="panel-body">
                <p class="m-0">No hay zonas asignadas aún.</p>
            </div>
        </div>

    @endif

</div>

@stop
