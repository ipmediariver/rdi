@component('mail::layout')
    @slot('header')
        @component('mail::header', ['url' => url('/')])
            Reporte diario de Mensajes
        @endcomponent
    @endslot

    <table>
    @foreach ($zones as $zone)
        @php
            $messages = $zone->messages()->whereDate('created_at', date('Y-m-d'))->get();
        @endphp
        @if ($messages->count() > 0)
            <tr>
                <td><h3 style="font-size: 20px; margin: 20px 0 20px 0 !important;">{!! $zone->name !!}</h3></td>
            </tr>

            @foreach ($messages as $message)
                <tr>
                    <td>
                        <a href="{{ route('show-message', ['zone' => $zone->zone_id, 'message' => $message->message_id]) }}" >
                            <b>{!! $message->subject !!}</b>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        {!! str_limit($message->description,140) !!}
                    </td>
                </tr>
                <tr>
                    <td>
                        <hr>
                    </td>
                </tr>

            @endforeach

        @endif

    @endforeach
    </table>

    @slot('footer')
        @component('mail::footer')
            © {{ date('Y-m-d') }} {{ config('app.name') }}. RDI
        @endcomponent
    @endslot
@endcomponent