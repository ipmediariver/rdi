@component('mail::layout')
    @slot('header')
        @component('mail::header', ['url' => url('/')])
            Incidente de {!! $category->name !!}
        @endcomponent
    @endslot

    <table>
        <tr>
            <td><h3 style="font-size: 20px; margin: 20px 0 20px 0 !important;">{!! $message->zone->name !!}</h3></td>
        </tr>
        <tr>
            <td>
                <a href="{{ route('show-message', ['zone' => $message->zone_id, 'message' => $message->message_id]) }}" >
                    <b>{!! $message->subject !!}</b>
                </a>
            </td>
        </tr>
        <tr>
            <td>
                {!! str_limit($message->description,140) !!}
            </td>
        </tr>
    </table>

    @slot('footer')
        @component('mail::footer')
            © {{ date('Y-m-d') }} {{ config('app.name') }}. RDI
        @endcomponent
    @endslot
@endcomponent