@extends('layouts.app')


@section('content')
	<div class="row">
		<div class="col-sm-4 col-sm-offset-4">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Error 404</h3>
				</div>
				<div class="panel-body">
					<p class="m-0">La página que intentas buscar no existe, por favor regresa al <a href="{{ route('dashboard') }}">inicio</a>.</p>
				</div>
			</div>
		</div>
	</div>
@stop