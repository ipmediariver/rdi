<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Laravel') }}</title>
        <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">
        
        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
        <link href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    </head>
    <body>
        <div>
            <nav class="navbar navbar-default navbar-static-top h_master">
                <div class="container d-flex align-items-center">
                    
                    <div class="navbar-header">
                        <!-- Collapsed Hamburger -->
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        
                        @if(auth()->user())
                        @if(count(auth()->user()->unreadNotifications) > 0)
                        <span class="notifications_count">
                            {{ count(auth()->user()->unreadNotifications) }}
                        </span>
                        @endif
                        @endif

                        </button>
                        <!-- Branding Image -->
                        <a class="navbar-brand" href="{{ url('/') }}">
                            <img src="{{ asset('img/logo.svg') }}" alt="{{ config('app.name') }}">
                        </a>
                    </div>
                    
                    <div class="collapse navbar-collapse ml-auto" id="app-navbar-collapse">
                        <!-- Right Side Of Navbar -->
                        <ul class="nav navbar-nav navbar-right">
                            <!-- Authentication Links -->
                            @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Ingresar</a></li>
                            @else
                            
                                <li>
                                    <a href="{{ route('zones') }}/">Zonas</a>
                                </li>
                                
                                @if(auth()->user()->role == 'admin')
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        Usuarios <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a href="{{ route('admins') }}">Administradores</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('agents') }}/">Agentes</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('contacts') }}/">Contactos</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="{{ route('categories') }}/">Categorías</a>
                                </li>
                                {{-- <li>
                                    <a href="{{ route('departments') }}/">Departamentos</a>
                                </li> --}}
                                <li>
                                    <a href="{{ route('reports') }}/">Reportes</a>
                                </li>
                                @endif
                                
                                @if(auth()->user()->role == 'contact')

                                <li>
                                    <a href="{{ route('reports') }}/">Reportes</a>
                                </li>
    
                                @endif


                                <li>
                                    <a href="{{ route('notifications') }}/">
                                        @if(count(auth()->user()->unreadNotifications) > 0)
                                        <span class="label label-default">{{ count(auth()->user()->unreadNotifications) }}</span>
                                        @endif
                                        Notificaciones
                                    </a>
                                </li>
                                
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a href="{{ route('profile') }}">
                                                Perfil
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                                Cerrar Sesión
                                            </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </li>

                            @endif
                        </ul>
                    </div>
                </div>
            </nav>
            
            @yield('checkin')
            
            <div class="container" style="min-height: 70vh">
                @if (session('msg'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ session('msg') }}
                </div>
                @endif
                @isset ($errors)
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                        <p class="m-0">{{ $error }}</p>
                        @endforeach
                    </div>
                    @endif                    
                @endisset
                @yield('content')
                
                @yield('extra-content')
            </div>
            
            <footer class="f_master">
                <div class="container">
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <a href="#">Inicio</a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#">Soporte Técnico</a>
                        </li>
                    </ul>
                    <p class="m-0">©{{ \Carbon\Carbon::now()->format('Y') .' '. config('app.name')}}</p>
                </div>
            </footer>
        </div>
        
        <script src="{{ asset('js/app.js') }}"></script>
        
        @if( auth()->user() )
        
        <script src="{{ asset('js/general.js') }}"></script>
        <script src="{{ asset('js/vue.js') }}"></script>
        <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('js/moment/moment.js') }}"></script>
        <script src="{{ asset('js/bootstrap-datetimepicker.js') }}"></script>

        @endif
        
        @yield('scripts')
    </body>
</html>