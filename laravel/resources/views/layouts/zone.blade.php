@extends('layouts.app')

@if( auth()->user()->role == 'agent' )

	@section('checkin')
		@include('zones.messages.partials.check-in')
	@stop

@endif

@section('content')

	<div class="row">
		
		<div class="col-sm-3">
			<div class="panel panel-default zone-info">
				
				<div class="panel-heading">
					<h3 class="panel-title">Información de Zona</h3>
				</div>
				
				<div class="panel-body zone-info-block">
					<p class="lead" data-zonename>
						<a class="text-dark" href="{{ route('show-zone', $zone->zone_id) }}/">
							{{ $zone->name }}
						</a>
					</p>
					<p class="m-0" data-zoneaddress>{{ $zone->address }}</p>
				</div>
				
				<ul class="list-group">
					@role('admin')
					<li class="list-group-item">
						<a href="{{ route('messages', $zone->zone_id) }}/">
							<i class="fa fa-comment"></i>
							Mensajes
						</a>
					</li>
					<li class="list-group-item">
						<a href="{{ route('zone-agents', ['zone' => $zone->zone_id]) }}/">
							<i class="fa fa-user"></i>
							Agentes
						</a>
					</li>
					<li class="list-group-item">
						<a href="{{ route('zone-contacts', ['zone' => $zone->zone_id]) }}/">
							<i class="fa fa-users"></i>
							Contactos
						</a>
					</li>
					<li class="list-group-item">
						<a href="{{ route('tasks', ['zone' => $zone->zone_id]) }}/">
							<i class="fa fa-bars"></i>
							Tareas
						</a>
					</li>
					<li class="list-group-item">
						<a href="{{ route('checks', ['zone' => $zone->zone_id]) }}/">
							<i class="fa fa-map-marker"></i>
							Check In
						</a>
					</li>
					{{-- <li class="list-group-item">
						<a href="{{ route('guests', ['zone' => $zone->zone_id]) }}/">
							<i class="fa fa-calendar-o"></i>
							Visitas
						</a>
					</li> --}}
					@endrole
					@role('agent')
						<li class="list-group-item">
						    <a href="{{ route('messages', $zone->zone_id) }}/">
						    	<i class="fa fa-comment"></i>
						    	Mensajes
						    </a>
						</li>
						<li class="list-group-item">
							<a href="{{ route('zone-contacts', ['zone' => $zone->zone_id]) }}/">
								<i class="fa fa-users"></i>
								Contactos
							</a>
						</li>
						<li class="list-group-item">
							<a href="{{ route('tasks', ['zone' => $zone->zone_id]) }}/">
								<i class="fa fa-bars"></i>
								Tareas
							</a>
						</li>
						{{-- <li class="list-group-item">
							<a href="{{ route('guests', ['zone' => $zone->zone_id]) }}/">
								<i class="fa fa-calendar-o"></i>
								Visitas
							</a>
						</li> --}}
					@endrole
					@role('contact')
						<li class="list-group-item">
						    <a href="{{ route('messages', $zone->zone_id) }}/">Mensajes</a>
						</li>
						<li class="list-group-item">
						    <a href="{{ route('zone-contacts', ['zone' => $zone->zone_id]) }}/">Contactos</a>
						</li>
						{{-- <li class="list-group-item">
						    <a href="{{ route('guests', ['zone' => $zone->zone_id]) }}/">Visitas</a>
						</li> --}}
						<li class="list-group-item">
							<a href="{{ route('tasks', ['zone' => $zone->zone_id]) }}/">
								<i class="fa fa-bars"></i>
								Tareas
							</a>
						</li>
					@endrole
				</ul>
				
				@role('admin')
					<div class="panel-footer">
						<a href="{{ route('edit-zone', $zone->zone_id) }}/">
							<i class="fa fa-pencil fa-fw"></i>
							Editar Información
						</a>
					</div>
				@endrole

			</div>
		</div>

		<div class="col-sm-9">
			@yield('right-content')
		</div>

	</div>
	
@stop