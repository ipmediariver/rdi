@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-sm-8 col-sm-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading d-flex align-items-center d-flex-mobile">
                <h3 class="panel-title">
                    Notificaciones
                </h3>
                <a href="{{ route('mark_all_notifications_as_read') }}" class="btn btn-primary btn-sm ml-auto">Marcar todo leido</a>
            </div>
            @php
				$notifications = auth()->user()->notifications()->whereNull('read_at')->get();
			@endphp
			@if(count($notifications) > 0)
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <tbody>
                        @foreach($notifications as $notification)
                        <tr>
                            <td class="text-center" width="20%">
                                {{ $notification->created_at->format('d M, Y') }}
                            </td>
                            <td>
                                @if($notification->data['notification_type'] == 'Nuevo Mensaje')
                                    <a href="#" class="m-0 notifications_anchor" onclick="read_notification($(this))" urlgo="{{ $notification->data['link'] }}" urlnotifications="{{ route('mark_notifications_as_read', ['notification' => $notification->id ]) }}">
                                        {{ str_limit($notification->data['subject'], 70) }}
                                    </a>
                                    <p class="m-0 text-muted">
                                        <small>
                                            {{ str_limit($notification->data['description'], 120) }}
                                        </small>
                                    </p>
                                @elseif($notification->data['notification_type'] == 'Mensaje Verificado')
                                    <p class="m-0">
                                        {{ $notification->data['notification'] }}
                                        <a href="#" class="notifications_anchor" onclick="read_notification($(this))" urlgo="{{ $notification->data['link'] }}" urlnotifications="{{ route('mark_notifications_as_read', ['notification' => $notification->id ]) }}">
                                            Ver mensaje
                                        </a>
                                    </p>
                                {{-- @elseif($notification->data['notification_type'] == 'Mensaje Severidad Alta')
                                    <p class="m-0">
                                        Mensaje Severidad Alta
                                    </p>
                                    <a href="#" class="m-0 notifications_anchor" onclick="read_notification($(this))" urlgo="{{ $notification->data['link'] }}" urlnotifications="{{ route('mark_notifications_as_read', ['notification' => $notification->id ]) }}">
                                        {{ str_limit($notification->data['subject'], 70) }}
                                    </a>
                                    <p class="m-0 text-muted">
                                        <small>
                                            {{ str_limit($notification->data['description'], 120) }}
                                        </small>
                                    </p> --}}
                                @elseif($notification->data['notification_type'] == 'Nueva Tarea')
                                    <p class="m-0">
                                        {{ $notification->data['notification'] }}
                                        <a href="#" class="notifications_anchor" onclick="read_notification($(this))" urlgo="{{ $notification->data['link'] }}" urlnotifications="{{ route('mark_notifications_as_read', ['notification' => $notification->id ]) }}">
                                            Ver tarea
                                        </a>
                                    </p>
                                @elseif($notification->data['notification_type'] == 'Nuevo Comentario')
                                    <a href="#" class="m-0 notifications_anchor" onclick="read_notification($(this))" urlgo="{{ $notification->data['link'] }}" urlnotifications="{{ route('mark_notifications_as_read', ['notification' => $notification->id ]) }}">
                                        {{ str_limit($notification->data['subject'], 70) }}
                                    </a>
                                    <p class="m-0 text-muted">
                                        <small>
                                            {{ str_limit($notification->data['description'], 120) }}
                                        </small>
                                    </p>
                                @elseif($notification->data['notification_type'] == 'Mensaje Verificado')
                                    <p class="m-0">
                                        {{ $notification->data['notification'] }}
                                        <a href="#" class="notifications_anchor" onclick="read_notification($(this))" urlgo="{{ $notification->data['link'] }}" urlnotifications="{{ route('mark_notifications_as_read', ['notification' => $notification->id ]) }}">
                                            Ver mensaje
                                        </a>
                                    </p>
                                @elseif($notification->data['notification_type'] == 'Nueva Tarea')
                                    <p class="m-0">
                                        {{ $notification->data['notification'] }}
                                        <a href="#" class="notifications_anchor" onclick="read_notification($(this))" urlgo="{{ $notification->data['link'] }}" urlnotifications="{{ route('mark_notifications_as_read', ['notification' => $notification->id ]) }}">
                                            Ver tarea
                                        </a>
                                    </p>
                                @elseif($notification->data['notification_type'] == 'Usuario Asignado Zona')
                                    <p class="m-0">
                                        <a href="#" class="notifications_anchor" onclick="read_notification($(this))" urlgo="" urlnotifications="{{ route('mark_notifications_as_read', ['notification' => $notification->id ]) }}">
                                        {{ $notification->data['subject'] }}
                                        </a>
                                    </p>
                                    <p>
                                        <small>
                                            {{ $notification->data['description'] }}
                                        </small>
                                    </p>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @else
            <div class="panel-body">
                No hay notificationes.
            </div>
            @endif
        </div>
    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
    /*$('.notifications_anchor').on('click', function(e){
			e.preventDefault()
			var url = $(this).attr('urlgo')
			
			$.ajax({
				type: 'get',
				url : $(this).attr('urlnotifications'),
				dataType: 'json'
			}).done(function(response){
				if (response.result) {
					window.location = url
				}
			})
		})*/

		function read_notification(anchor){
			var urlgo 			 = anchor.attr('urlgo')
			var urlnotifications = anchor.attr('urlnotifications')
			
			// var urls = [
			// 	'http://rdi.proteusconsulting.com',
			// 	'http://localhost.com/',
			// 	'http://localhost/',
			// ]
			
			// $.each(urls,function(idx, url){
			// 	urlgo = urlgo.replace( url, '{{ url('/') }}')
			// 	urlnotifications = urlnotifications.replace(url, '{{ url('/') }}')
			// })

			// alert(urlnotifications)
			// alert(urlgo)
			// return 

			$.ajax({
				type: 'get',
				url : urlnotifications,
				dataType: 'json'
			}).done(function(response){
				if (response.result) {
                    if (urlgo != '') {
                        window.location.href = urlgo
                    } else {
                        window.location.reload()
                    }
				}
			})
		}
</script>
@stop
