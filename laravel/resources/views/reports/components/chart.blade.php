<div class="panel panel-default">
	<div class="panel-heading d-flex align-items-center">
		
		<h3 class="panel-title">
			{{ $title }}
		</h3>

		<div style="width: 50%" class="ml-auto text-right">
			<div class="checkbox m-0">
				<label>
					<input type="checkbox" class="chart_checkbox" @change="include_in_report" rel="{{ $id }}"> Incluir en descarga
				</label>
			</div>
		</div>
	</div>
	<div class="panel-body" id="{{ $id }}">
		
		{{-- Aqui se despliega la grafica.... --}}

	</div>
</div>