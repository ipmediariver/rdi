@extends('layouts.app')

@section('content')

	
	<div id="reports_app">


		<div class="panel panel-default">
			<div class="panel-heading d-flex align-items-center">
				<h3 class="panel-title">
					Generar Reporte
				</h3>
				

				<div v-if="report_loaded" class="checkbox m-0">
					<label style="white-space: nowrap;" class="d-flex align-items-center">
						
						<input type="checkbox" class="my-0" id="all_charts_checkbox" style="position: relative; top: -1px; right: 5px;" @change="include_all_charts_checkbox"> 

						Incluir todas las gráficas

					</label>
				</div>
				
				<form 
					v-if="report_loaded" 
					action="{{ route('download_report') }}" 
					method="post"
					style="margin-left: 15px;" 
					@submit.prevent="submit_report">
					
			
					{{ csrf_field() }}
		

					<input type="hidden" name="report" :value="report_json()">


					<button class="btn btn-default btn-sm ml-auto">
						
						<i class="fa fa-download"></i> Descargar Reporte

					</button>


				</form>


			</div>
			<div class="panel-body" style="overflow: visible;">
				<form action="{{ route('generate_report') }}" @submit.prevent="generate_report" method="post">


					{{ csrf_field() }}
					
					<div class="row d-flex align-items-end">
						<div class="col-sm-2">
							<div class="form-group m-0">
								<label> Zona: </label>
								<select name="zone_id" id="" class="form-control" required>
									<option disabled selected value>Elige una zona</option>
									@php
										if (auth()->user()->role == 'contact') {
											$zones = auth()->user()->contact->zones();
										} else {
											$zones = \App\Models\Zones\Zone::get();
										}
									@endphp
									@foreach($zones as $zone)
										<option value="{{ $zone->zone_id }}">{{ $zone->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-sm-2">
							<label>Categorías</label>
							<select name="category_id" class="form-control" @change="change_category">
								<option value="">Todas</option>
								@foreach (\App\Models\Zones\Category::get()->sortBy('fullName') as $category)
									<option value="{{ $category->category_id }}">{{ $category->fullName }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-sm-2">
							<label>Estatus Mensaje</label>
							<select name="verified" class="form-control" @change="change_verified">
								<option value="">Todos</option>
								<option value="1">Verificados</option>
								<option value="0">No verificados</option>
							</select>
						</div>
						<div class="col-sm-2">
							<label>Fecha de inicio</label>
							<div class="form-group m-0">
								<input type="text" name="report_start" placeholder="Fecha de inicio" class="form-control datepicker" required autocomplete="off" value="{{ date('d-m-Y',strtotime('-7 day')) }}">
							</div>
						</div>
						<div class="col-sm-2">
							<label>Fecha final</label>							
							<div class="form-group m-0">
								<input type="text" name="report_end" placeholder="Fecha final" class="form-control datepicker" required autocomplete="off" value="{{ date('d-m-Y') }}">
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group m-0">
								<button class="btn btn-success btn-block">
									<i class="fa fa-line-chart"></i>
									Generar Reporte
								</button>
							</div>
						</div>
					</div>

				</form>
			</div>
		</div>

		
		<p v-cloak v-if="loading_report" class="text-center">Cargando Reporte...</p>


		<div v-cloak v-if="report_loaded">
			
			<div class="row">
				<div class="col-sm-7">

					@component('reports.components.chart', [

						'id' => 'activity_by_category',
						'title' => 'Actividad Diaria Por Categoría'

					])

					@endcomponent
		

				</div>


				<div class="col-sm-5">
					
			
					@component('reports.components.chart', [

						'id' => 'total_activity_by_category',
						'title' => 'Incidentes Por Categoría'

					])

					@endcomponent



				</div>
			</div>

			@if (auth()->user()->role != 'contact')
				

				@component('reports.components.chart', [

					'id' => 'activity_by_agent',
					'title' => 'Actividad Por Agente'

				])

				@endcomponent


			@endif


			@component('reports.components.chart', [

				'id' => 'activity_by_schedule',
				'title' => 'Incidentes Por Horario'

			])

			@endcomponent

			@if (auth()->user()->role != 'contact')
				@component('reports.components.chart', [

					'id' => 'total_verified_messages',
					'title' => 'Incidentes Verificados'

				])

				@endcomponent

			@endif

			@component('reports.components.chart', [

				'id' => 'total_severity_messages',
				'title' => 'Incidentes por Severidad'

			])
			@endcomponent
			
		</div>


	</div>


@stop



@section('scripts')


	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>

	<script>


			var reportsApp = new Vue({

			
				el: '#reports_app',

				data: {

					range: {

						start: null,
						end: null

					},
					zone_id: null,
					category_id: null,
					verified: null,
					loading_report: false,
					report_loaded: false,
					report_sections: [],
					report_charts: [],
					first_render: [],
					include_all_charts: false,


				},

				mounted: function(){


				},

				methods: {


					include_all_charts_checkbox: function(e){


						var t = this;


						var checkbox = $(e.target);


						var is_checked = checkbox.is(':checked');


						var charts_checkbox = $('.chart_checkbox');


						t.report_sections = [];


						if(is_checked){


							t.include_all_charts = true;

							
							charts_checkbox.each(function(){


								var checkbox = $(this);

								
								var rel = checkbox.attr('rel');


								checkbox.prop('checked', false).attr('disabled', 'disabled');


								t.report_sections.push(rel);


							});


						}else{


							t.include_all_charts = false;

							t.report_sections = [];


							charts_checkbox.each(function(){

								$(this).removeAttr('disabled');

							});

						}


					},


					all_charts: function(){


						var t = this;

						var checkbox = $('#all_charts_checkbox');


						if(t.include_all_charts){


							checkbox.attr('checked', 'checked');


						}else{


							checkbox.removeAttr('checked');


						}


					},


					submit_report: function(e){


						var t = this;


						var form = $(e.target);

						var btn = $('button', form);


						if(t.include_all_charts == true || t.report_sections.length > 0){


							form.submit();


						}else{


							alert('Selecciona las graficas que deseas incluir en el reporte');

							setTimeout(function(){

								btn.removeAttr('disabled');

							}, 150);


						}



					},


					report_json: function(){

						var t = this;

						var charts = [];


						$.each(t.report_sections, function(i, o){


							$.each(t.report_charts, function(index, chart){


								if(o == chart.title){


									charts.push(chart);


								}


							});


						});


						var report = {


							charts: charts,
							range: t.range,
							zone_id: t.zone_id,
							category_id : t.category_id,
							verified : t.verified


						}

						return JSON.stringify(report);

					},


					include_in_report: function(e){


						var t = this;

						var checkbox = $(e.target);

						var checked = checkbox.is(':checked');

						var id = checkbox.attr('rel');


						if(checked){


							t.report_sections.push(id);


						}else{


							t.report_sections.splice(id, 1);

						}


					},


					generate_report: function(e){

						var t = this;

						var form = $(e.target);

						var btn = $('button', form);

						var url = form.attr('action');

						var data = form.serialize();

						t.loading_report = true;

						t.report_charts = [];

						t.first_render = [];

						$.post(url, data, function(response){

							
							t.loading_report = false;

							t.report_loaded = true;

							t.range.start = response.range.report_start;
							t.range.end = response.range.report_end;
							t.zone_id = response.zone_id;

							btn.removeAttr('disabled');


						}).done(function(response){


							setTimeout(function(){

								$.each(response, function(i, o){



									var container = $('#' + i);


									var chart_size = 65;

									
									if(i == 'activity_by_category'){

										chart_size = 110;

									}

									
									if(i == 'total_activity_by_category'){

										chart_size = 160;

									}


									var canvas = `<canvas id="` + i + `_canvas" height="`+chart_size+`">`;


									container.html('');


									container.append(canvas);


									ctx = $('#' + i + '_canvas');



									var render_chart_fn = {


										animation: {


											onComplete: function(e){
												// var chartInstance = this.chart,
												// ctx = chartInstance.ctx;

												// ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
												// ctx.textAlign = 'center';
												// ctx.textBaseline = 'bottom';

												// this.data.datasets.forEach(function(dataset, i) {
												// 	var meta = chartInstance.controller.getDatasetMeta(i);
												// 	meta.data.forEach(function(bar, index) {
												// 		var data = dataset.data[index];
												// 		ctx.fillText(data, bar._model.x, bar._model.y - 10);
												// 	});
												// });

												if($.inArray(i, t.first_render) == -1){


													var render_image = this.toBase64Image();


													var chart = {


														title: i,
														image: render_image


													}


													t.first_render.push(i);


													t.report_charts.push(chart);


												}
											}

										}

									}


									$.extend(o.options, render_chart_fn);


									var report_chart = new Chart(ctx, o);


								});


							}, 150);


						});


					},

					change_category(){
						this.category_id = $('[name="category_id"]').val()
					},
					change_verified(){
						this.verified = $('[name="verified"]').val()
					}


				}


			});


	</script>



@stop