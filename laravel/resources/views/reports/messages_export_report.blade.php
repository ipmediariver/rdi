<style>
    
    
    html,body{

        padding: 0;
        margin: 0;

    }

    body{

        font-family: 'Helvetica', sans-serif;
        font-size: 9px;
        position: relative;
        padding-top: 100px;

    }
    

    h2.header_title{

        font-weight: lighter;
        font-size: 22px !important;
        color: #fff;
        position: fixed;
        top: 10px;
        right: 20px;
        z-index: 1;
        text-align: right;
        display: inline-block;
        width: 100%;

    }


    h3{

        font-weight: lighter;
        font-size: 18px !important;
        text-transform: uppercase;
        color: #13395f;

    }


    h4{

        font-weight: lighter;
        font-size: 14px !important;
        text-transform: uppercase;
        color: #13395f;
        margin: 0 0 15px 0;

    }

    .header_img{

    
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        z-index: -1;
        

    }

    .full_content{
        padding: 20px;
    }
    
    .page-break {
        
        page-break-after: always;

    }

</style>


<img src="{{ asset('img/pdf_header.jpg') }}" class="header_img" alt="">


<h2 class="header_title">

    Reporte De Zona {{ $zone->name }}

</h2>


<div class="full_content">
    

    <h4 style="text-align: center;">
        Reporte de {{ $report->range->start }} a {{ $report->range->end }}
    </h4>

    <table width="100%">
        @foreach ($report->charts as $chart)
            
            
            @php


                $width = 0;
                $title = null;

                switch ($chart->title) {
                    case 'activity_by_category':
                        
                        $width = 580;
                        $title = 'Actividad Diaria Por Categoría';


                        break;


                    case 'total_activity_by_category':
                        
                        $width = 400;
                        $title = 'Incidentes Por Categoría';


                        break;


                    case 'activity_by_agent':
                        
                        $width = 715;
                        $title = 'Actividad Por Agente';


                        break;


                    case 'activity_by_schedule':
                        
                        $width = 715;
                        $title = 'Incidentes Por Horario';


                        break;


                    case 'total_verified_messages':
                        
                        $width = 715;
                        $title = 'Incidentes Verificados';


                        break;

                    case 'total_severity_messages':
                        
                        $width = 715;
                        $title = 'Incidentes Por Severidad';


                        break;                    

                }


            @endphp


            <tr>
                <td align="center">
                    
                    <h3>{{ $title }}</h3>

                </td>
            </tr>
            

            <tr>
                <td align="center">
                    
                    <img src="{{ $chart->image }}" style="width: {{ $width }}px">

                </td>

            </tr>


        @endforeach
    </table>
    

    {{-- <div class="page-break"></div>

        <table width="100%">
            @foreach(\App\Models\Zones\Category::get() as $category)
        
                @php 

                    $percent = $category->percent($zone->zone_id);

                @endphp
                <tr>
                    <th>{{ $category->fullName }} ({{ $percent }}%)</th>
                </tr>
                <tr>
                    <td>
                        <div class="progress" style="height: 5px; @if($loop->last) margin-bottom: 0; @endif">
                            <div class="progress-bar" role="progressbar" style="width: {{ $percent }}%; background-color: {{ $category->color  }}" aria-valuenow="{{ $percent }}" aria-valuemin="0" aria-valuemax="100">
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
        </table>
     --}}
     <div class="page-break"></div>

    <table width="100%" cellpadding="5" cellspacing="0" border="1" style="margin-bottom: 30px;">
        @php
            $categories_data = \App\Models\Zones\Category::get();
            $stadistics = [];
            foreach ($categories_data as $cat){
                $stadistics['categories'][ $cat->fullName ]['total_messages'] = 0;
                $stadistics['categories'][ $cat->fullName ]['verified'] = 0;
                $stadistics['categories'][ $cat->fullName ]['not_verified'] = 0;
                $stadistics['categories'][ $cat->fullName ]['low'] = 0;
                $stadistics['categories'][ $cat->fullName ]['middle'] = 0;
                $stadistics['categories'][ $cat->fullName ]['high'] = 0;
                $stadistics['categories'][ $cat->fullName ]['color'] = $cat->color;
            }

            foreach ($messages as $message) {
                if (isset($message->category)) {

                    $stadistics['categories'][ $message->category->fullName ]['total_messages']++;

                    if ($message->verified) {
                        $stadistics['categories'][ $message->category->fullName ]['verified']++;
                    } else{
                        $stadistics['categories'][ $message->category->fullName ]['not_verified']++;
                    }

                    if ($message->severity->name == 'Baja') {
                        $stadistics['categories'][ $message->category->fullName ]['low']++;
                    } elseif ($message->severity->name == 'Media') {
                        $stadistics['categories'][ $message->category->fullName ]['middle']++;
                    } elseif ($message->severity->name == 'Alta') {
                        $stadistics['categories'][ $message->category->fullName ]['high']++;
                    }

                }
            }

        @endphp

        <tr>
            <th>Categorías</th>
            {{-- <th>Verificados</th>
            <th>No Verificados</th> --}}
            <th>Total</th>
            <th>Severidad Baja</th>
            <th>Severidad Media</th>
            <th>Severidad Alta</th>
        </tr>
        @foreach ($stadistics['categories'] as $category => $sts_cat)
            <tr>
                <th style="border-right: 3px solid {{ $sts_cat['color'] }}">
                    {{ $category }}
                </th>
                {{-- <td align="center">
                    {{ $sts_cat['verified'] }}
                </td>
                <td align="center">
                    {{ $sts_cat['not_verified'] }}
                </td> --}}
                <td align="center">
                    {{ $sts_cat['total_messages'] }}
                </td>
                <td align="center">
                    {{ $sts_cat['low'] }}
                </td>
                <td align="center">
                    {{ $sts_cat['middle'] }}
                </td>
                <td align="center">
                    {{ $sts_cat['high'] }}
                </td>
            </tr>
        @endforeach

    </table>
    {{-- <div class="page-break"></div> --}}

    <table width="100%" cellpadding="5" cellspacing="0" border="1">
        <thead>
            <tr>
                <th>Asunto</th>
                <th>Descripción</th>
                <th>Categoría</th>
                @if (auth()->user()->role != 'contact')
                    @if (!$report->verified)
                        <th>Verificado</th>
                    @endif
                @endif
                <th>Agente</th>
                <th>Severidad</th>
                <th>Fecha</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($messages as $message)
                <tr>
                    <td>
                        <a href="{{ route('show-message', [$zone, $message]) }}">
                            {{ $message->subject }}
                        </a>
                    </td>
                    <td>{{ $message->description }}</td>

                    <td style="background-color: {{ $message->category ? $message->category->color : '#ffffff' }}">
                        
                        @if($message->category && $message->category->fullName == 'Otros')

                
                            <span style="color: #ffffff">
                                
                                {{ $message->category ? $message->category->fullName : 'Sin Categoría' }}

                            </span>


                        @else


                            {{ $message->category ? $message->category->fullName : 'Sin Categoría' }}


                        @endif


                    </td>
                    @if (auth()->user()->role != 'contact')
                        @if (!$report->verified)
                            <td style="background-color: {{ ($message->verified) ? '#5bce58' : '#ed6161' }}">
                                {{ ($message->verified)?'Si':'No' }}
                            </td>
                        @endif
                    @endif
                    <td>{{ $message->author->user->name or 'Sin agente' }}</td>
                    <td style="background-color: {{ $message->severity->color }}">
                        {{ $message->severity->name }}
                    </td>
                    <td>{{ date('d M, Y H:i', strtotime($message->created_at)) }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

</div>