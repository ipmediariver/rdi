@extends('layouts.app')
@section('content')
<div class="row">
    <div class="col-sm-4 col-sm-offset-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                Llena los siguientes campos para crear un nuevo administrador.
                </h3>
            </div>
            <div class="panel-body">
                <p><small>(*) Campos requeridos</small></p>
                <form action="{{ route('store-admin') }}" method="POST">
                    @include('users.partials.form-edit-user')
                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Guardar Administrador</button>
                </form>
            </div>
        </div>
    </div>
</div>
@stop