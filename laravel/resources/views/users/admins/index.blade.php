@extends('layouts.app')
@section('content')
<div class="panel panel-default mb-0">
    <div class="panel-heading d-flex align-items-center d-flex-mobile">
        <h3 class="panel-title">Administradores</h3>
        @role('admin')
            <a href="{{ route('new-admin') }}/" class="btn btn-primary btn-sm ml-auto">Nuevo</a>
        @endrole
    </div>
    @if(count( $users ) > 0)
        <div class="table-responsive">
            <table class="table table-bordered mb-0">
                <thead>
                    <tr>
                        <th>Nombre del administrador</th>
                        <th>Nombre de usuario</th>
                        <th>Correo electrónico</th>
                        <th width="20%" class="text-center">Detalles</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $admin)
                    <tr>
                        <td>
                            {{ ucwords($admin->name) }}
                        </td>
                        <td>
                            {{ $admin->username }}
                        </td>
                        <td>
                            {{ $admin->email }}
                        </td>
                        <td width="20%" class="text-center">
                            <a href="{{ route('show-admin', $admin->id) }}/">Ver detalles</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @else
        <div class="panel-body">
            <p class="m-0">No se han registrado administradores aún</p>
        </div>
    @endif
</div>

{{ $users->links() }}

@stop