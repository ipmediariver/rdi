@extends('layouts.app')
@section('content')
<div class="panel panel-default">
    <div class="panel-heading d-flex align-items-center">
        <h3 class="panel-title">
            Información del administrador
        </h3>
        @role('admin')
        <a href="{{ route( 'edit-admin', $user->id ) }}/" class="btn btn-sm btn-primary ml-auto">
            Editar administrador
        </a>
        @endrole
    </div>
    <table class="table table-bordered table-striped">
        <tbody>
            <tr>
                <td width="20%">Nombre completo</td>
                <td>{{ $user->name }}</td>
            </tr>
            <tr>
                <td width="20%">Usuario</td>
                <td>{{ $user->username }}</td>
            </tr>
            <tr>
                <td>Correo electrónico</td>
                <td>{{ $user->email }}</td>
            </tr>
        </tbody>
    </table>
</div>
@stop
