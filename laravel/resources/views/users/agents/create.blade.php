@extends('layouts.app')
@section('content')
<div class="row">
	<div class="col-sm-4 col-sm-offset-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">
				Llena los siguientes campos para crear un nuevo agente.
				</h3>
			</div>
			<div class="panel-body">
				<p><small>(*) Campos requeridos</small></p>
				<form action="{{ route('store-agent') }}" method="POST">

					@if(request('rel_zone'))
						<input type="hidden" value="{{ request('rel_zone') }}" name="rel_zone">
					@endif

					@include('users.partials.form-edit-user')
					<button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Guardar Agente</button>
				</form>
			</div>
		</div>
	</div>
</div>
@stop