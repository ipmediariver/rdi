@extends('layouts.app')
@section('content')
<div class="panel panel-default mb-0">
	<div class="panel-heading d-flex align-items-center d-flex-mobile">
		<h3 class="panel-title">Agentes</h3>
		@role('admin')
			<a href="{{ route('new-agent') }}/" class="btn btn-primary btn-sm ml-auto">Nuevo</a>
		@endrole
	</div>
	@if(count($agents) > 0)
		<div class="table-responsive">
			<table class="table table-bordered mb-0">
				<thead>
					<tr>
						<th>Nombre del agente</th>
						<th>Nombre de usuario</th>
						<th>Correo electrónico</th>
						<th width="20%" class="text-center">Detalles</th>
					</tr>
				</thead>
				<tbody>
					@foreach($agents as $agent)
					<tr>
						<td>
							{{ ucwords($agent->user->name) }}
						</td>
						<td>
							{{ $agent->user->username }}
						</td>
						<td>
							{{ $agent->user->email }}
						</td>
						<td width="20%" class="text-center">
							<a href="{{ route('show-agent', $agent->agent_id) }}/">Ver detalles</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	@else
		<div class="panel-body">
			<p class="m-0">No se han registrado agentes aún</p>
		</div>
	@endif
</div>

{{ $agents->links() }}

@stop