@extends('layouts.app')
@section('content')
<div class="panel panel-default">
	<div class="panel-heading d-flex align-items-center">
		<h3 class="panel-title">
			Información del agente
		</h3>
		@role('admin')
		<a href="{{ route('edit-agent', $agent->agent_id) }}/" class="btn btn-sm btn-primary ml-auto">
			Editar agente
		</a>
		@endrole
	</div>
	<table class="table table-bordered table-striped">
		<tbody>
			<tr>
				<td width="20%">Nombre completo</td>
				<td>{{ $agent->user->name }}</td>
			</tr>
			<tr>
				<td width="20%">Nombre de usuario</td>
				<td>{{ $agent->user->username }}</td>
			</tr>
			<tr>
				<td>Correo electrónico</td>
				<td>{{ $agent->user->email }}</td>
			</tr>
		</tbody>
	</table>
</div>
@stop