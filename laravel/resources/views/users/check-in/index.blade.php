@extends('layouts.zone')

@section('right-content')

<div class="panel panel-default">
    <div class="panel-heading d-flex align-items-center d-flex-mobile">
        
        <h3 class="panel-title">Check In</h3>
    </div>
    @if(count($checks) > 0)
    <div class="table-responsive m-0">
        <table class="table table-bordered table-striped m-0">
            <thead>
                <tr>
                    <th>Agente</th>
                    <th width="25%">Entrada</th>
                    <th width="25%">Salida</th>
                    @if (auth()->user()->role == 'admin')
                    <th></th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @foreach($checks as $check)
                <tr>
                    <td>
                        {{$check->agent->user->name or 'Agente Eliminado'}}
                    </td>
                    <td>
                        {{ date( 'd/m/Y H:i:s', strtotime( $check->check_in ) ) }}
                    </td>
                    <td>
                        @if ($check->check_out)
                            {{ date( 'd/m/Y H:i:s', strtotime( $check->check_out ) ) }}
                        @else
                            pendiente
                        @endif
                    </td>
                    @if (auth()->user()->role == 'admin')
                    <th>
                        <form action="{{ route('delete-checkin', ['zone' => $zone->zone_id, 'checkin' => $check->id ]) }}" method="POST">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('¿Estas seguro que deseas eliminar el registro?')">
                                Eliminar check-in
                            </button>
                        </form>
                    </th>
                    @endif
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    {{$checks->links()}}
    @else
    <div class="panel-body">
        <p>No se han registrado check in aún.</p>
    </div>
    @endif
</div>

@stop