@extends('layouts.app')
@section('content')

<div class="row">
	<div class="col-sm-4 col-sm-offset-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Editar Contacto</h3>
			</div>
			<div class="panel-body">
				<p><small>(*) Campos requeridos</small></p>
				<form action="{{ route('update-contact', $contact->contact_id) }}" id="updateContactForm" method="POST">
					{{ method_field('PATCH') }}
					@include('users.partials.form-edit-user')
				</form>
				<form action="{{ route('delete-contact', $contact->contact_id) }}" id="deleteContactForm" method="POST">
					{{ method_field('DELETE') }}
					{{ csrf_field() }}
				</form>
				<button type="submit" class="btn btn-primary" form="updateContactForm">
					Guardar Cambios
				</button>
				<button type="submit" form="deleteContactForm" class="btn btn-danger" onclick="return confirm('¿Estas seguro que deseas eliminar este contacto?')">
					Eliminar contacto
				</button>
			</div>
		</div>
	</div>
</div>

@stop