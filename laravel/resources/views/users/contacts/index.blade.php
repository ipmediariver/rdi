@extends('layouts.app')
@section('content')
<div class="panel panel-default mb-0">
	<div class="panel-heading d-flex align-items-center d-flex-mobile">
		
		<h3 class="panel-title">Contactos</h3>
		
		@role('admin')
		<a href="{{ route('new-contact') }}/" class="btn btn-primary btn-sm ml-auto">Nuevo</a>
		@endrole

	</div>
	@if(count($contacts) > 0)
		<div class="table-responsive">
			<table class="table table-bordered mb-0">
				<thead>
					<tr>
						<th>Nombre del contacto</th>
						<th>Nombre de usuario</th>
						<th>Correo electrónico</th>
						<th width="20%" class="text-center">Detalles</th>
					</tr>
				</thead>
				<tbody>
					@foreach($contacts as $contact)
					<tr>
						<td>
							{{ ucwords($contact->user->name) }}
						</td>
						<td>
							{{ $contact->user->username }}
						</td>
						<td>
							{{ $contact->user->email }}
						</td>
						<td width="20%" class="text-center">
							<a href="{{ route('show-contact', $contact->contact_id) }}/">Ver detalles</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	@else
	<div class="panel-body">
		<p class="m-0">No se han registrado contactos aún</p>
	</div>
	@endif
</div>

{{ $contacts->links() }}

@stop