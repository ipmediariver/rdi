@extends('layouts.app')
@section('content')
<div class="panel panel-default">
	<div class="panel-heading d-flex align-items-center">
		<h3 class="panel-title">Información del contacto</h3>
		@role('admin')
		<a href="{{ route('edit-contact', $contact->contact_id) }}/" class="btn btn-primary btn-sm ml-auto">
			Editar
		</a>
		@endrole
	</div>
	<table class="table table-bordered table-striped">
		<tbody>
			<tr>
				<td width="25%">Nombre</td>
				<td>{{ $contact->user->name }}</td>
			</tr>
			<tr>
				<td width="25%">Nombre de usuario</td>
				<td>{{ $contact->user->username }}</td>
			</tr>
			<tr>
				<td width="25%">Correo Electrónico</td>
				<td>{{ $contact->user->email }}</td>
			</tr>
		</tbody>
	</table>
</div>
@stop