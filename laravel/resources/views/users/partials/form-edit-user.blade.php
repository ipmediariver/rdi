@isset ($contact)
    @php
        $user = $contact->user;
    @endphp
@endisset

@isset ($agent)
    @php
        $user = $agent->user;
    @endphp
@endisset

{{ csrf_field() }}
<div class="form-group">
    <label>* Nombre completo:</label>
    <input type="text" class="form-control" name="name" value="{{ $user->name or old('name') }}" required autofocus>
</div>
<div class="form-group">
    <label>* Nombre usuario:</label>
    <input type="text" class="form-control" name="username" value="{{ $user->username or old('username') }}" required>
</div>
<div class="form-group">
    <label>* Correo electrónico:</label>
    <input type="text" class="form-control" name="email" value="{{ $user->email or old('email') }}" required>
</div>
@if ( !empty( $user ) )
    <div class="form-group">
        <label>Contraseña nueva *</label>
        <input type="password" name="password" class="form-control">
    </div>
    <div class="form-group">
        <label>Confirmar contraseña *</label>
        <input type="password" name="password_confirmation" class="form-control">
    </div>
@endif