@extends('layouts.zone')

@section('right-content')

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Agentes en <a href="{{ route('show-zone', $zone->zone_id) }}">{{$zone->name}}</a></h3>		
	</div>
	<div class="panel-body">
		<form action="{{ route('assign-agent-to-zone', $zone->zone_id) }}" method="POST">
			
			{{ csrf_field() }}

			<div class="form-group">
				<label>Asignar agente:</label>
				<div class="d-flex">
					<select name="agent" class="form-control mb-xs-1">
						<option value="null" style="display: none">Selecciona un agente</option>
						@foreach(App\Models\Users\Agent::latest()->get() as $agent)
						
							<option value="{{ $agent->agent_id }}">{{ $agent->user->name }}</option>

						@endforeach
					</select>
					<button type="submit" class="btn btn-primary mx-10 mx-xs-10 mb-xs-1 btn-xs-block">
						Asignar Agente
					</button>
					<a href="{{ route('new-agent') }}?rel_zone={{ $zone->zone_id }}" class="ml-2 btn btn-default btn-xs-block">
						Nuevo Agente
					</a>
				</div>
			</div>

		</form>
		@if(count($agents) > 0)
		<ul class="list-group mb-0">
			@foreach($agents as $assigned)
				
				<li class="list-group-item d-flex align-items-center text-xs-center">
					<p class="m-0">
						<a href="{{ route('show-agent', $assigned->agent->agent_id) }}/">{{ ucwords($assigned->agent->user->name) }}</a>
					</p>
					<form action="{{ route('unassign-agent-from-zone', ['zone' => $zone->zone_id, 'agent' => $assigned->agent->agent_id]) }}" method="POST" class="ml-auto">
						{{ method_field('DELETE') }}
						{{ csrf_field() }}
						<button class="btn btn-link btn-xs-block" onclick="return confirm('¿Estas seguro que deseas eliminar a este agente de esta zona?')">
							Desasignar
						</button>
					</form>
				</li>

			@endforeach
		</ul>
		{{$agents->links()}}
		@else
		<p class="m-0">No hay agentes asignados a esta zona</p>
		@endif
	</div>
</div>

@stop