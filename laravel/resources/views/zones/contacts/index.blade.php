@extends('layouts.zone')
@section('right-content')
<div class="panel panel-default" id="contacts_app">
	<div class="panel-heading d-flex align-items-center d-flex-mobile">
		<h3 class="panel-title">
		Contactos en <a href="{{ route('show-zone', $zone->zone_id) }}">{{$zone->name}}</a>
		</h3>
	</div>
	<div class="panel-body">
		
		@role('admin')
		<form action="{{ route('assign-contact-to-zone', $zone->zone_id) }}" method="POST">
			{{ csrf_field() }}
			<div class="form-group">
				<label>Asignar contacto:</label>
				<div class="d-flex">
					<select name="contact" class="form-control mb-xs-1" required>
						<option style="display: none">Selecciona un contacto</option>
						@foreach(App\Models\Users\Contact::latest()->get() as $contact)
						<option value="{{ $contact->contact_id }}">{{ $contact->user->name }}</option>
						@endforeach
					</select>
					<button type="submit" class="btn btn-primary mx-10 mx-xs-10 mb-xs-1 btn-xs-block" style="margin:0 10px">
					Asignar Contacto
					</button>
					<a href="{{ route('new-contact') }}?rel_zone={{ $zone->zone_id }}" class="ml-2 btn btn-default btn-xs-block">
						Nuevo Contacto
					</a>
				</div>
			</div>
		</form>
		@endrole
		
		@if(count($contacts) > 0)
		<ul class="list-group mb-0">
			@foreach($contacts as $assigned)
			
			<li class="list-group-item d-flex align-items-center">
				<p class="m-0">
					<a href="{{ route('show-contact', $assigned->contact->contact_id) }}/">
						
						{{ ucwords($assigned->contact->user->name) }}
					</a>
				</p>
				<div class="ml-auto d-flex align-items-center">
					
					<a href="#" class="btn btn-link" style="padding: 0; margin-right: 1rem;" @click.prevent="get_contact_categories('{{ $assigned->contact->contact_id }}')">
						
						[ Categorías asignadas ]
					</a>

					@role('admin')
					
					<form action="{{ route('unassign-contact-from-zone', ['zone' => $zone->zone_id, 'contact' => $assigned->contact->contact_id]) }}" method="POST" class="ml-3">
						{{ method_field('DELETE') }}
						{{ csrf_field() }}
						<button class="btn btn-link" style="padding: 0" onclick="return confirm('¿Estas seguro que deseas eliminar a este contacto de esta zona?')">
						[ Desasignar ]
						</button>
					</form>
					@endrole
				</div>
			</li>
			
			@endforeach
		</ul>
		@else
		<p class="m-0">No hay contactos asignados a esta zona</p>
		@endif
	</div>

	
	<!-- Modal -->
	<div class="modal fade" id="notificationsModal" tabindex="-1" role="dialog" aria-labelledby="notificationsModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="notificationsModalLabel">
						
						Asignar Categorias

					</h4>
				</div>
				<div class="modal-body">
					
						<p>
							<b>Selecciona las categorías de incidentes de las que deba ser notificado el contacto.</b>
						</p>

						
						<form v-if="contact" id="contactCategoriesForm" :action="categories_contact_form_route()" method="post">
							
							{{ csrf_field() }}

							@foreach($categories as $category)

							
							<div class="checkbox">
								<label>
									
									<input type="checkbox" name="categories[]" data-categoryid="{{$category->category_id}}" value="{{ $category->category_id }}"> 

									{{$category->fullName}}

								</label>
							</div>


							@endforeach

						</form>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<button type="submit" form="contactCategoriesForm" class="btn btn-primary">Guardar</button>
				</div>
			</div>
		</div>
	</div>


</div>

@stop


@section('scripts')

	<script>
		
		var contactsApp = new Vue({

			el: '#contacts_app',

			data: {

				contact: null,
				loading_contact: false

			},

			mounted: function(){


			},

			methods: {


				get_contact_categories: function(contact_id){

					var t = this;

					var link = $(event.target);

					link.text('[ Cargando... ]');

					var route = "{{ route('show-contact', '#contact_id') }}";

					route = route.replace('#contact_id', contact_id);

					t.loading_contact = true;

					$.getJSON(route, function(response){

						t.contact = response;
						
						$('#notificationsModal').modal('show');

						link.text('[ Categorías asignadas ]');

						setTimeout(function(){

							$.each(t.contact.categories_contact, function(i, o){

								$('[data-categoryid="'+o.category_id+'"]').attr('checked', 'checked');

							});

						}, 300);

					});

					$('#notificationsModal').on('hidden.bs.modal', function (e) {
					
						$('[data-categoryid]').removeAttr('checked');

					});


				},

				categories_contact_form_route: function(){

					var t = this;

					var route = "{{ route('zones.contacts.categories.store', [ $zone->zone_id, '#contact_id' ]) }}";

					route = route.replace('#contact_id', t.contact.contact_id);

					return route;

				}


			}

		});

	</script>

@stop