@extends('layouts.app')
@section('content')
<div class="row">
	<div class="col-sm-4 col-sm-offset-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Llena los siguientes campos para crear una nueva zona</h3>
			</div>
			<div class="panel-body">
				<p><small>(*) Campos requeridos</small></p>
				<form action="{{ route('store-zone') }}" method="POST" id="newZoneForm">
					@include('zones.partials.form')
				</form>
				<button type="submit" form="newZoneForm" class="btn btn-primary">
					Guardar Zona
				</button>			
			</div>
		</div>
	</div>
</div>
@stop