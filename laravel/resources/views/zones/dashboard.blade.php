@extends('layouts.app')
@section('content')
<div class="panel panel-default">
	<div class="panel-heading d-flex align-items-center">
		<h3 class="panel-title">RDI - Reporte De Incidentes</h3>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-3">
				<div class="panel panel-default panel-stadistic m-0">
					<div class="panel-body">
						<h2 class="m-0">{{ sprintf('%04d', App\Models\Security\Zone::get()->count()) }}</h2>
						<a href="{{ route('zones') }}/" class="m-0">Zonas</a>
						<i class="fa fa-map-marker"></i>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="panel panel-default panel-stadistic m-0">
					<div class="panel-body">
						<h2 class="m-0">{{ sprintf('%04d', App\Models\Users\Contact::get()->count()) }}</h2>
						<a href="{{ route('contacts') }}" class="m-0">Contactos</a>
						<i class="fa fa-comment-o"></i>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="panel panel-default panel-stadistic m-0">
					<div class="panel-body">
						<h2 class="m-0">{{ sprintf('%04d', App\Models\Users\Agent::get()->count()) }}</h2>
						<a href="{{ route('agents') }}" class="m-0">Agentes</a>
						<i class="fa fa-user"></i>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="panel panel-default panel-stadistic m-0">
					<div class="panel-body">
						<h2 class="m-0">{{ sprintf('%04d', App\Models\Security\Category::get()->count()) }}</h2>
						<a href="#" class="m-0">Categorías</a>
						<i class="fa fa-bars"></i>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop