@extends('layouts.app')
@section('content')
<div class="content">
    <div class="col-sm-6 col-sm-offset-3">
        <form action="{{ route('store-department') }}" method="POST" id="newZoneForm">
            {{ csrf_field() }}
            <div class="panel panel-default">
                <div class="panel-heading d-flex align-items-center">
                    <h3 class="panel-title">Departamentos</h3>
                </div>
                <div class="panel-body">
                    @include('zones.departments.partials.form-department')
                </div>
                <div class="panel-footer">
                    <button class="btn btn-primary">
                        Crear
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@stop