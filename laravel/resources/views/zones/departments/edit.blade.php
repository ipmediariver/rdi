@extends('layouts.app')
@section('content')
<div class="content">
    <div class="col-sm-6 col-sm-offset-3">
        <form action="{{ route( 'update-department', [ 'department' => $department->id ] ) }}/" method="POST">
            {{ method_field('PATCH') }}
            {{ csrf_field() }}
            <div class="panel panel-default">
                <div class="panel-heading d-flex align-items-center">
                    <h3 class="panel-title">Departamentos</h3>
                </div>
                <div class="panel-body">
                    @include('zones.departments.partials.form-department')
                </div>
                <div class="panel-footer">
                    <button class="btn btn-primary">Actualizar</button>
                    <button type="submit" class="btn btn-danger" form="deleteZoneForm" onclick="return confirm('Estas seguro que deseas eliminar este departamento?')">Eliminar</button>
                </div>
            </div>
        </form>

        <form action="{{ route('delete-department', $department->id) }}" method="POST" id="deleteZoneForm">
            {{ method_field('DELETE') }}
            {{ csrf_field() }}
        </form>
    </div>
</div>
@stop