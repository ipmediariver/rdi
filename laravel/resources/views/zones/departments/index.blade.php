@extends('layouts.app')

@section('content')
    <div class="content">
        <div class="panel panel-default">
            <div class="panel-heading d-flex align-items-center d-flex-mobile">
                <h3 class="panel-title">Departamentos</h3>
                @if(Auth::user()->role == 'admin')
                <a href="{{ route('new-department') }}" class="btn btn-primary btn-sm ml-auto">
                    Nuevo
                </a>
                @endif
            </div>
            @if ( count( $departments ) > 0 )
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Descripción</th>
                                <th class="text-center">Detalles</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ( $departments as $department )
                                <tr>
                                    <td>{{$department->name}}</td>
                                    <td>{{str_limit($department->description, 70)}}</td>
                                    <td class="text-center">
                                        <a href="{{ route('edit-department', ['department' => $department->id ]) }}">
                                            Ver detalles
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>                
                </div>
                <div class="panel-body">
                    {{ $departments->links() }}
                </div>
            @else
                <div class="panel-body">
                    <p class="m-0">No se han registrado departamentos aún</p>
                </div>
            @endif
        </div>
    </div>
@stop