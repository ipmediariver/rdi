
<div class="row">
    <div class="col-xs-12">
        <div class="form-group">
            <label>Nombre</label>
            <input type="text" name="name" class="form-control" value="{{ $department->name or old('name') }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="form-group">
            <label>Descripción</label>
            <input type="text" name="description" class="form-control" value="{{ $department->description or old('description') }}">
        </div>
    </div>
</div>