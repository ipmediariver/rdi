@extends('layouts.app')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading d-flex align-items-center">
        <h3 class="panel-title">Información de visita</h3>
        @if(Auth::user()->role != 'contact')
        <a href="{{ route('edit-department', ['department' => $department->id]) }}" class="btn btn-primary btn-sm ml-auto">
            Editar Visita
        </a>
        @endif
    </div>
        
    <table class="table table-bordered table-striped">
        <tbody>
            <tr>
                <td>Departamento</td>
                <th>{{$department->name}}</th>
            </tr>
            <tr>
                <td>Descripción</td>
                <th>{{$department->description}}</th>
            </tr>
        </tbody>
    </table>
@stop