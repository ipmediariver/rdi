@extends('layouts.zone')

@section('right-content')

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Editar Zona</h3>		
	</div>
	<div class="panel-body">
		<p><small>(*) Campos requeridos</small></p>
		<form action="{{ route('update-zone', $zone->zone_id) }}" method="POST">
			{{ method_field('PATCH') }}
			@include('zones.partials.form')
			<button type="submit" class="btn btn-primary">
				Guardar Cambios
			</button>
			<button type="submit" class="btn btn-danger" form="deleteZoneForm" onclick="return confirm('¿Estas seguro que deseas eliminar esta zona?')">
				Eliminar Zona
			</button>
		</form>
		<form action="{{ route('delete-zone', $zone->zone_id) }}" method="POST" id="deleteZoneForm">
			{{ method_field('DELETE') }}
			{{ csrf_field() }}
		</form>
	</div>
</div>

@stop