@extends('layouts.zone')

@section('right-content')

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Llena los siguientes campos para crear una nueva visita</h3>
	</div>
	<div class="panel-body">
		<p><small>(*) Campos requeridos</small></p>
		<form action="{{ route('store-guest', $zone->zone_id) }}/" method="POST" enctype="multipart/form-data" id="newGuestForm">
			@include('zones.guests.partials.form')
		</form>
		<button type="submit" form="newGuestForm" class="btn btn-primary">
			Guardar Visita
		</button>
	</div>
</div>

@stop