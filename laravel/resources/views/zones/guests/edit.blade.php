@extends('layouts.zone')

@section('right-content')

<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Editar Visita</h3>
	</div>

	<div class="panel-body">
		<p><small>(*) Campos requeridos</small></p>
		<form action="{{ route('update-guest', ['zone' => $zone->zone_id, 'guest' => $guest->guest_id]) }}/" id="editGuestForm" method="POST">
			{{ method_field('PATCH') }}
			
			@include('zones.guests.partials.form')
		</form>
		<form action="{{ route('delete-guest', ['zone' => $zone->zone_id, 'guest' => $guest->guest_id]) }}" id="deleteGuestForm" method="POST">
			{{ method_field('DELETE') }}
			{{ csrf_field() }}
		</form>
		<button type="submit" form="editGuestForm" class="btn btn-primary">
			Guardar Cambios
		</button>
		<button type="submit" form="deleteGuestForm" class="btn btn-danger" onclick="return confirm('¿Estas seguro que deseas eliminar esta visita?')">
			Eliminar Visita
		</button>
	</div>

</div>

@stop