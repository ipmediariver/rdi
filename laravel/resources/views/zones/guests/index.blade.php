@extends('layouts.zone')

@section('right-content')

<div class="panel panel-default mb-0">
	<div class="panel-heading d-flex align-items-center d-flex-mobile">
		<h3 class="panel-title">ADVIS</h3>
		@if(Auth::user()->role != 'contact')
		<a href="{{ route('new-guest', $zone->zone_id) }}/" class="btn btn-primary btn-sm ml-auto">
			Nueva visita
		</a>
		@endif
	</div>
	@if(count($guests) > 0)
		<div class="table-responsive">
			<table class="table table-bordered table-striped mb-0">
				<thead>
					<tr>
						<th style="vertical-align: middle;">Nombre</th>
						<th style="vertical-align: middle;">Departamento</th>
						<th style="vertical-align: middle;">Anfitrión</th>
						<th style="white-space: nowrap; vertical-align: middle;">Entrada</th>
						<th style="white-space: nowrap; vertical-align: middle;">Salida</th>
						<th>Total</th>
						<th style="vertical-align: middle;" class="text-center">Detalles</th>
					</tr>
				</thead>
			<tbody>
				@foreach($guests as $guest)
					<tr>
						<td style="vertical-align: middle;">{{ $guest->name }}</td>
						<td style="vertical-align: middle;">{{ $guest->department->name }}</td>
						<td style="vertical-align: middle;">{{ $guest->host }}</td>
						<td style="vertical-align: middle; text-align: center;">
							{{ date('d M  Y H:i', strtotime( $guest->start_time ) ) }}
						</td>
						<td style="vertical-align: middle; text-align: center;">
							@if($guest->end_time)

								{{ date('d M  Y H:i', strtotime( $guest->end_time ) ) }}

							@else
								
								<form action="{{ route('update-end-time', ['zone' => $zone->zone_id, 'guest' => $guest->guest_id]) }}" method="POST">

									{{ method_field('PATCH') }}

									{{ csrf_field() }}

									<button type="submit" class="btn btn-default btn-sm">
										Establecer hora
									</button>

								</form>

							@endif
						</td>
						<td style="white-space: nowrap; vertical-align: middle;">
							@if($guest->end_time)
								{{ $guest::diff_guest_time( $guest->start_time, $guest->end_time ) }}
							@else
								Pendiente
							@endif
						</td>
						<td style="white-space: nowrap; vertical-align: middle;" class="text-center"><a href="{{ route('show-guest', ['zone' => $zone->zone_id, 'guest' => $guest->guest_id]) }}">Ver detalles</a></td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	@else
		<div class="panel-body">
			<p class="m-0">No se han agregado visitas.</p>
		</div>
	@endif
</div>

{{ $guests->links() }}

@stop