{{ csrf_field() }}


<div class="row">
	<div class="col-sm-12">
		<div class="form-group">
			<label for="">* Nombre Completo:</label>
			<input type="text" name="name" class="form-control" value="{{ $guest->name or old('name') }}" required autofocus>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			<label for="">* Departamento:</label>
			<select class="form-control" name="department_id" required>
				@foreach ( $departments as $department )
				<option value="{{$department->department_id}}">{{$department->name}}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group">
			<label for="">Anfitrión:</label>
			<input type="text" name="host" class="form-control" value="{{ $guest->host or old('host') }}">
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			<label for="">* Motivo de visita:</label>
			<input type="text" name="subject" class="form-control" value="{{ $guest->subject or old('subject') }}" required>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group">
			<label for="">* Género:</label>
			<select name="gender" class="form-control">
				<option value="m">Masculino</option>
				<option value="f">Femenino</option>
			</select>
		</div>
	</div>
</div>

<div class="form-group">
	<label for="">Detalles de visita:</label>
	<textarea name="description" class="form-control" rows="4">{{ $guest->description or old('description') }}</textarea>
</div>

<div class="row">
	<div class="col-sm-4">
		<div class="form-group">
			<label for="">* Hora de entrada:</label>
			<input type="text" name="start_time" class="form-control time"  value="{{ $guest->start_time or old('start_time') }}" required>
		</div>
	</div>
	<!-- <div class="col-sm-6">
		<div class="form-group">
			<label for="">Hora de salida:</label>
			<input type="text" name="end_time" class="form-control time" value="{{ $guest->end_time or old('end_time') }}">
		</div>
	</div> -->
	<div class="col-sm-4">
		<div class="form-group">
			<label for="">* Identificación:</label>
			<select name="id_type" class="form-control" required>
				<option value="id" {{ isset($guest) ? $guest->id_type == 'id' ? 'selected' : '' : '' }}>INE credencial para votar</option>
				<option value="license" {{ isset($guest) ? $guest->id_type == 'license' ? 'selected' : '' : '' }}>Licencia de conducir</option>
				<option value="passport" {{ isset($guest) ? $guest->id_type == 'passport' ? 'selected' : '' : '' }}>Pasaporte</option>
				<option value="other" {{ isset($guest) ? $guest->id_type == 'other' ? 'selected' : '' : '' }}>Otro</option>
			</select>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="form-group">
			<label for="">* Número de identificación:</label>
			<input type="text" class="form-control" name="id_number" value="{{ $guest->id_number or old('id_number') }}" required>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div class="form-group">
			<label for="">* Huella digital o identificación por imagen</label>
			<input type="file" name="finger_print" >
		</div>
	</div>
</div>
