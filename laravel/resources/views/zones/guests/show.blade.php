@extends('layouts.zone')

@section('right-content')

<div class="panel panel-default">
	<div class="panel-heading d-flex align-items-center">
		<h3 class="panel-title">Información de visita</h3>
		@if(Auth::user()->role != 'contact')
		<a href="{{ route('edit-guest', ['zone' => $zone->zone_id, 'guest' => $guest->guest_id]) }}/" class="btn btn-primary btn-sm ml-auto">
			Editar Visita
		</a>
		@endif
	</div>
		
	<table class="table table-bordered table-striped">
		<tbody>
			@if ( $guest->finger_print )
			<tr>
				<td colspan="2" style="text-align: center;">
					<img src="{{ url('/').'/'.$guest->finger_print}}" width="300px" height="300px">					
				</td>
			</tr>
			@endif	
			<tr>
				<th width="25%">Nombre de visita</th>
				<td>{{ $guest->name }}</td>
			</tr>
			<tr>
				<th>Departamento</th>
				<td>{{ $guest->department->name }}</td>
			</tr>
			<tr>
				<th>Anfitrión</th>
				<td>{{ $guest->host }}</td>
			</tr>
			<tr>
				<th>Motivo de visita</th>
				<td>{{ $guest->subject }}</td>
			</tr>
			<tr>
				<th>Hora de entrada</th>
				<td>{{ date('d M Y H:i:s', strtotime( $guest->start_time ) ) }}</td>
			</tr>
			<tr>
				<th>Hora de salida</th>
				<td>
					@if($guest->end_time)
													
						{{ date('d M Y H:i:s', strtotime( $guest->end_time ) ) }}

					@else
						
						<form action="{{ route('update-end-time', ['zone' => $zone->zone_id, 'guest' => $guest->guest_id]) }}" method="POST">

							{{ method_field('PATCH') }}

							{{ csrf_field() }}

							<button type="submit" class="btn btn-default btn-sm">
								Establecer hora
							</button>

						</form>

					@endif
				</td>
			</tr>
			<tr>
				<th>Total de tiempo</th>
				<td>
					@if($guest->end_time)
					{{ $guest::diff_guest_time( $guest->start_time, $guest->end_time ) }}
					@else
					Pendiente
					@endif
				</td>
			</tr>
			<tr>
				<th>Identificación</th>
				<td>
					
					@php
						switch ($guest->id_type) {
							case 'license':
								$id_type = 'Licencia de conducir';
								break;
							case 'passport':
								$id_type = 'Pasaporte';
								break;
							case 'other':
								$id_type = 'Otra identificación';
								break;
							
							default:
								$id_type = 'Credencial para votar';
								break;
						}
					@endphp
					{{ $id_type }}
				</td>
			</tr>
			<tr>
				<th>Número de identificación</th>
				<td>{{ $guest->id_number }}</td>
			</tr>
		</tbody>
	</table>
</div>

@stop