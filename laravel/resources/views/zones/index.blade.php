@extends('layouts.app')

@section('content')

<div class="panel panel-default">
	<div class="panel-heading d-flex align-items-center d-flex-mobile">
		
		<h3 class="panel-title">Zonas</h3>
	
		@role('admin')

			<a href="{{ route('new-zone') }}/" class="btn btn-primary btn-sm ml-auto">
				Nueva
			</a>

		@endrole

	</div>
	@if(count($zones) > 0)
	<div class="table-responsive m-0">
		<table class="table table-bordered table-striped m-0">
			<thead>
				<tr>
					<th>Nombre de la zona</th>
					<th>Fecha</th>
					<th width="15%" class="text-center">Detalles</th>
				</tr>
			</thead>
			<tbody>
				@foreach($zones as $zone)
				<tr>
					<td>
						{{ $zone->name }}

						@role('agent')
							@php
	    						$check = $zone->checkIns()->where('agent_id', auth()->user()->agent->agent_id )->where('check_out', null)->first();
							@endphp
						@endrole
					</td>
					<td>{{ $zone->created_at->format('d M, Y') }}</td>
					<td width="15%" class="text-center">
						<a href="{{ route('show-zone', $zone->zone_id) }}/">Ver detalles</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	@else
	<div class="panel-body">
		<p class="m-0">No se han registrado zonas aún.</p>
	</div>
	@endif
</div>

@stop