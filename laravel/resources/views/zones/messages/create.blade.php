@extends('layouts.zone')

@section('right-content')

	
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Crear nuevo mensaje</h3>
		</div>
		<div class="panel-body">
			
			<form action="{{ route('store-message', $zone->zone_id) }}" method="POST" enctype="multipart/form-data">
				
				@include('zones.messages.partials.form')

				<button type="submit" class="btn btn-primary">Publicar Mensaje</button>

			</form>

		</div>
	</div>
	

@stop