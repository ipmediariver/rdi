@extends('layouts.zone')

@section('right-content')

	<div class="panel panel-default" id="message_app">
		<div class="panel-heading">
			<h3 class="panel-title">Editar mensaje</h3>
		</div>
		<div class="panel-body">
			
			<form action="{{ route('update-message', ['zone' => $zone->zone_id, 'message' => $message->message_id]) }}" method="POST" enctype="multipart/form-data">

				{{ method_field('PATCH') }}
				
				@include('zones.messages.partials.form')

				<button type="submit" class="btn btn-primary">Guardar Cambios</button>
				<button type="submit" class="btn btn-danger" onclick="return confirm('¿Estas seguro que deseas eliminar este mensaje?')" form="deleteMessageForm">Eliminar Mensaje</button>

			</form>

			<form id="deleteMessageForm" action="{{ route('delete-message', ['zone' => $zone->zone_id, 'message' => $message->message_id]) }}" method="POST">
				{{ method_field('DELETE') }}
				{{ csrf_field() }}
			</form>

		</div>
	</div>

@stop

@section('scripts')

	@php 

		$images = $message->images ? json_decode($message->images) : [];

	@endphp

	<script>
		var vm = new Vue({

			data: {

				images: [

					@foreach($images as $img)
						@php
							$have_url = strrpos($img, "http");
							if ($have_url === false) {
								$img = url('/').'/uploads7e8r2t58d/'.$img;
							}
							$img = str_replace('https://v2-rdi.proteusconsulting.com/', 'http://v2.rdimx.com/', $img);
						@endphp

						'{{ $img }}',

					@endforeach	

				]

			},

			mounted: function(){

			},

			computed: {

				current_images: function(){

					var t = this;

					return JSON.stringify(t.images);

				}

			},

			methods: {

				remove_image_from_message: function(i){

					this.images.splice(i, 1);

				}

			}

		}).$mount('#message_app');
	</script>

@stop