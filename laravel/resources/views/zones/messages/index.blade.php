@extends('layouts.zone')

@section('right-content')

	<div class="panel panel-default mb-0">
		<div class="panel-heading d-flex align-items-center d-flex-mobile">
			<h3 class="panel-title">
				Mensajes
			</h3>
			<a href="{{ route('new-message', $zone->zone_id) }}/" class="btn btn-primary btn-sm ml-auto new_msg_btn">
				<i class="fa fa-pencil"></i>
				Nuevo Mensaje
			</a>
		</div>
		<div class="panel-body">

			@if(count($messages) > 0)

			<div class="row row_sm d-flex align-items-stretch flex-wrap">
				@foreach($messages as $message)
					@include('components.message_card')
				@endforeach
			</div>

			@else
	
			<p class="m-0">No se han agregado mensajes a esta zona</p>

			@endif
		</div>
		<div class="panel-body">
			{{ $messages->links() }}
		</div>
	</div>
@stop

@section('scripts')

	<script>
		$(document).ready(function(){

			$('.owl-carousel').owlCarousel({
			    loop:true,
			    margin:0,
			    nav:false,
			    dots: true,
			    items: 1
			})

		});
	</script>

@stop