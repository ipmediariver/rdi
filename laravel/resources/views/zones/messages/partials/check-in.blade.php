@if( empty( $checkin ) )

    @if ( isset( $check_not_close ) )

        <div class="check_in_box fixed text-center">
            <div class="container">
                <form action="{{ route('close-checkin', ['zone' => $check_not_close->zone_id, 'checkin' => $check_not_close->id]) }}" method="post">
                        
                    {{ csrf_field() }}

                    <input type="hidden" name="new_check_in" value="true">
                    
                    <button class="btn-checkout mr-4">
                        <i class="fa fa-power-off"></i> Check Out <br>
                        {{ $check_not_close->zone->name }}
                    </button>

                </form>
            </div>
        </div>    

    @else

        <div class="check_in_box fixed text-center">
            <div class="container">
                
                <form action="{{ route('create-checkin', $zone->zone_id) }}/" method="post">
                        
                    {{ csrf_field() }}

                    <p class="text-uppercase">Haz Check-In para iniciar tu turno</p>
                    
                    <button class="btn-checkin"><i class="fa fa-map-marker"></i> Check In</button>

                </form>
                
            </div>
        </div>    
    
    @endif

    
@else

    <div class="check_in_box text-center">
        <div class="container">
            
            <form action="{{ route('close-checkin', ['zone' => $zone->zone_id, 'checkin' => $checkin->id]) }}" method="post">
                    
                {{ csrf_field() }}
                
                <button class="btn-checkout mr-4"><i class="fa fa-power-off"></i> Check Out</button>

            </form>
            
        </div>
    </div>

@endif