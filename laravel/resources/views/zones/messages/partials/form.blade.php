{{ csrf_field() }}
<div class="row">
	<div class="col-sm-6">
		<div class="form-group">
			<label for="subject">
				Asunto
			</label>
			<input type="text" name="subject" value="{{ $message->subject or old('subject') }}" class="form-control" autofocus required>
		</div>
	</div>
</div>



<div class="row">
	
	
	<div class="col-sm-6">
		
		<div class="form-group">
			<label for="category_id">
				Categoría
			</label>
			<select name="category_id" class="form-control" required="required">
				<option disabled selected value="">Elíge una opción</option>
				@foreach($categories as $category)
					<option value="{{ $category->category_id }}" {{ isset($message) ? $message->category_id == $category->category_id ? 'selected' : '' : '' }}>
							
						{{ $category->fullName }}

					</option>

				@endforeach
			</select>
		</div>

	</div>


	<div class="col-sm-6">
		
		<div class="form-group">
			<label for="severity_id">
				Severidad
			</label>
			<select name="severity_id" class="form-control" required="required">
				<option disabled selected value="">Elíge una opción</option>
				@foreach(App\Models\Zones\Severity::get() as $severity)
					<option value="{{ $severity->id }}" {{ isset($message) ? $message->severity_id == $severity->id ? 'selected' : '' : '' }}>{{ $severity->name }}</option>
				@endforeach
			</select>
		</div>

	</div>


</div>



<div class="form-group">
	<label for="description">
		Descripción
	</label>
	<textarea name="description" rows="3" class="form-control" required>{{ $message->description or old('description') }}</textarea>
</div>

@isset($message)


<input type="hidden" name="current_images" :value="current_images">

<div class="form-group" v-if="images.length > 0">
	<ul class="list-inline mb-0">
		<li v-for="(image, index) in images">
			<div class="thumb_img" :style="{backgroundImage: 'url('+image+')'}">
				<a href="#" @click.prevent="remove_image_from_message(index)">
					<i class="fa fa-close"></i>
				</a>
			</div>
		</li>
	</ul>
</div>

@endisset

<div class="form-group">
	<label for="">Adjuntar Imágenes</label>
	<input type="file" name="images[]" multiple>
</div>