@extends('layouts.zone')

@section('right-content')

	
	<div class="panel panel-default">
		<div class="panel-heading d-flex align-items-center">
			<h3 class="panel-title mb-xs-1">
				Información del mensaje
			</h3>


			@role('admin')

			<div class="ml-auto d-flex align-items-center">
				
				@if(! $message->verified)

				<form action="{{ route('validate-message', ['zone' => $zone->zone_id, 'message' => $message->message_id]) }}/" method="POST" id="validateMessageForm">
					{{ csrf_field() }}
				</form>
				<button type="submit" 
						class="btn btn-sm btn-success mx-10 mx-xs-10"
						form="validateMessageForm"
						onclick="return confirm('¿Estas seguro que deseas marcar este mensaje como verificado?')" 
					>
					Validar Mensaje
				</button>
				@endif
				<a href="{{ route('edit-message', ['zone' => $zone->zone_id, 'message' => $message->message_id]) }}/" class="btn btn-sm btn-primary">
					Editar
				</a>
			</div>

			@endrole

		</div>
		<div class="panel-body">


			<p class="m-0">

				<i class="fa fa-circle" style="color: {{ $message->severity->color }}"></i>
			
				Severidad {{ $message->severity->name }}

			</p>

			<p class="m-0 text-uppercase" style="display: inline-block; font-weight: bold; color: {{ $message->category->color or ''  }}">

				<small>{{ $message->category->fullName or 'Sin categoría' }}</small>

			</p>


			@if($message->verified)
			<p class="text-success"><b><i class="fa fa-check"></i> Mensaje Verificado</b></p>
			@endif
			<p>
				<small>
					{{ $message->created_at->format('d M, Y - h:i a') }} |
					{{ $message->category->name or 'Sin categoría' }} | 
					<a href="{{ $message->author ? route('show-agent', $message->author->agent_id) : '#' }}/">
						{{ $message->author->user->name or 'Admin' }}
					</a> 
					dice...
				</small>
			</p>
			<p><b>{{ $message->subject }}</b></p>
			<p>{{ $message->description }}</p>
			@if($message->images)
			<ul class="list-inline mb-0">
				@foreach(json_decode($message->images) as $img)
				<li>
					@php
						$have_url = strrpos($img, "http");
						if ($have_url === false) {
							$img = url('/').'/uploads7e8r2t58d/'.$img;
						}
						$img = str_replace('https://v2-rdi.proteusconsulting.com/', 'http://v2.rdimx.com/', $img);
					@endphp
					<div class="thumb_img" style="background-image: url('{{ $img }}')" data-toggle="modal" data-target="#thumbImgModal" data-thumb="{{ $img }}"></div>
				</li>
				@endforeach
			</ul>
			@endif
		</div>
	</div>
	<div class="panel panel-default" id="messageComments">
		
		<ul class="list-group" v-if="comments.length > 0">
			<li class="list-group-item" v-for="comment in comments">
				<p class="m-0"><small><b>@{{ comment.author }}</b> | @{{ comment.date }}</small></p>
				<p class="m-0">@{{ comment.description }}</p>
			</li>
		</ul>

		<div class="panel-body">
			<form action="{{ route('create-message', ['zone' => $zone->zone_id, 'message' => $message->message_id] ) }}" method="POST" class="comments_form" @submit.prevent="add_comment">
				{{ csrf_field() }}
				<div class="form-group m-0">					
					<textarea class="form-control" name="message" placeholder="Agrega un comentario aquí" required></textarea>
				</div>
				<button type="submit" class="btn btn-primary btn-sm">
					<i class="fa fa-paper-plane"></i> Comentar
				</button>
			</form>

		</div>
	</div>
			


	@component('components.modal', [

		'id' => 'thumbImgModal',
		'title' => $message->subject

	])


	@slot('body')
	
		<div class="display_thumb_img"></div>

	@endslot

	@endcomponent

@stop

@section('scripts')

	<script src="{{ asset('js/messages.js') }}"></script>

	
	<script>
		var message_comments = new Vue({


			el: '#messageComments',


			data: {


				comments: []


			},


			mounted: function(){

				this.get_comments();

			},


			methods: {

				get_comments: function(){


					var t = this;

					var url = '{{ route('show-message', ['zone' => $zone->zone_id, 'message' => $message->message_id]) }}';

					$.getJSON(url, function(comments){


						t.comments = comments;


					});


				},

				add_comment: function(e){


					var t = this;

					var form = $(e.target);

					var url = form.attr('action');

					var data = form.serialize();

					var button = $('button', form);

					$.post(url, data, function(comment){

						button.removeAttr('disabled');

					}).done(function(comment){

						t.get_comments();

						form[0].reset();

					});


				}


			}


		})
	</script>



@stop