{{ csrf_field() }}

<div class="form-group">
	<label>* Nombre de la zona:</label>
	<input type="text" name="name" class="form-control" value="{{ $zone->name or old('name') }}" required autofocus>
</div>

<div class="form-group">
	<label>Dirección</label>
	<input type="text" name="address" class="form-control" value="{{ $zone->address or old('address') }}">
</div>