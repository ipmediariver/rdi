@extends('layouts.zone')
@section('right-content')


@role('admin')
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">
			Mensajes
		</h3>
	</div>
	<div class="panel-body">
		
		{{-- <canvas id="message_chart" height="104"></canvas> --}}

		
		{{-- 16 - Octubre - 2018			

		Estamos reemplazando la grafica 
		de las categorias por estadisticas por
		componentes de bootstrap (progress) --}}

		@foreach($categories as $category)
		
		@php 

			$percent = $category->percent($zone->zone_id);

		@endphp

		<b>{{ $category->fullName }} ({{ $percent }}%)</b>

		<div class="progress" style="height: 5px; @if($loop->last) margin-bottom: 0; @endif">
			
			<div 	class="progress-bar" 
					role="progressbar" 
					style="width: {{ $percent }}%; background-color: {{ $category->color  }}" 
					aria-valuenow="{{ $percent }}" 
					aria-valuemin="0" 
					aria-valuemax="100">
			</div>

		</div>

		@endforeach
	

	</div>
</div>
@endrole

@role('contact')
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">
			Mensajes
		</h3>
	</div>
	<div class="panel-body">
		<canvas id="message_chart" height="104"></canvas>
	</div>
</div>
{{-- <div class="row d-flex align-items-stretch">
	<div class="col-sm-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">
					Tiempo promedio de visitas
				</h3>
			</div>
			<div class="panel-body">
				<canvas id="visits_time_chart" height="104"></canvas>
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">
					Visitas por genero
				</h3>
			</div>
			<div class="panel-body">
				<canvas id="visits_gender_chart" height="104"></canvas>
			</div>
		</div>
	</div>
</div> --}}
@endrole
<div class="panel panel-default" style="height: 100%">
	<div class="panel-heading d-flex align-items-center">
		<h3 class="panel-title">Mensajes Recientes</h3>
		@role('admin')
		<a href="{{ route('new-message', $zone->zone_id) }}/" class="btn btn-primary btn-sm ml-auto">
			Nuevo
		</a>
		@endrole
	</div>
	@if(count($zone->messages) > 0)
	<ul class="list-group">
		@foreach($zone->messages()->take(4)->latest()->get() as $message)
		<li class="list-group-item">
			{{ $message->author->user->name or 'Admin' }} dice... 
			<a href="{{ route('show-message', ['zone' => $zone->zone_id, 'message' => $message->message_id]) }}">
				{{ str_limit($message->subject, 60) }}
			</a> | 
			{{ $message->created_at->format('d M, Y') }}
		</li>
		@endforeach
	</ul>
	@else
	<div class="panel-body">
		<p class="m-0">No hay mensajes</p>
	</div>
	@endif
	<div class="panel-footer">
		<a href="{{ route('messages', $zone->zone_id) }}/">
			Ver todos los mensajes <i class="fa fa-angle-right fa-fw"></i>
		</a>
	</div>
</div>

<div class="row d-flex align-items-stretch">
	<div class="col-sm-6">
		<div class="panel panel-default m-0 d-flex flex-column mb-xs-1" style="height: 100%">
			<div class="panel-heading d-flex align-items-center">
				<h3 class="panel-title">Agentes Asignados</h3>
				@role('admin')
				<a href="{{ route('zone-agents', $zone->zone_id) }}" class="btn btn-primary btn-sm ml-auto">
					Asignar
				</a>
				@endrole
			</div>
			@if(count($zone->agents) > 0)
			<ul class="list-group">
				@foreach($zone->agents()->take(4)->latest()->get() as $assigned)
				<li class="list-group-item">
					<a href="{{ route('show-agent', $assigned->agent->agent_id) }}/">
						{{ $assigned->agent->user->name }}
					</a>
				</li>
				@endforeach
			</ul>
			@else
			<div class="panel-body">
				<p class="m-0">No hay agentes asignados</p>
			</div>
			@endif
			<div class="panel-footer mt-auto">
				<a href="{{ route('zone-agents', $zone->zone_id) }}/">
					Ver todos los agentes asignados <i class="fa fa-angle-right fa-fw"></i>
				</a>
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="panel panel-default m-0 d-flex flex-column" style="height: 100%">
			<div class="panel-heading d-flex align-items-center">
				<h3 class="panel-title">Contactos Asignados</h3>
				@role('admin')
				<a href="{{ route('zone-contacts', $zone->zone_id) }}" class="btn btn-primary btn-sm ml-auto">
					Nuevo
				</a>
				@endrole
			</div>
			@if(count($zone->contacts) > 0)
			<ul class="list-group">
				@foreach($zone->contacts()->take(4)->latest()->get() as $assigned)
				<li class="list-group-item">
					<a href="{{ route('show-contact', $assigned->contact->contact_id) }}/">
						{{ $assigned->contact->user->name }}
					</a>
				</li>
				@endforeach
			</ul>
			@else
			<div class="panel-body">
				<p class="m-0">No hay contactos asignados</p>
			</div>
			@endif
			<div class="panel-footer mt-auto">
				<a href="{{ route('zone-contacts', $zone->zone_id) }}/">
					Ver todos los contactos asignados <i class="fa fa-angle-right fa-fw"></i>
				</a>
			</div>
		</div>
	</div>
</div>

@stop

@section('scripts')

	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>

	<script>
		$(document).ready(function(){


			function build_messages_by_category_chart( container = 'message_chart' ){
				@php
					$colors = [];
				@endphp
				var dataset = { 
					"message_chart" : [
			        	{
				            data: [
			        			@foreach( $categories as $category )
			            			@php
			            				$count = $zone
				            				->messages()
				            				->where('category_id', $category['id'])
				            				->get()
				            				->count();
				            			$colors[] = $category['color'];
			            			@endphp

			            			"{{ $count }}",
				        		@endforeach
				            ],
				            backgroundColor: {!! json_encode($colors) !!},
				            borderWidth: 1
				        },
				    ],
					@role('contact')
				    {{--"visits_time_chart" : [
			        	{
				            label: 'Tiempo promedio visitas',
				            data: [
				            	@for($i = 1; $i <= 7; $i++)
				            		@php
				            			$date = \Carbon\Carbon::now()->subDays(7)->addDays($i)->format('Y-m-d'); 
				            			$guest_data = App\Models\Zones\Guest::where('start_time', 'like', '%' . $date . '%')
				            				->where('zone_id', $zone->zone_id )
				            				->get();
				            			$total_hours_p_day = 0;
				            			$count_items_p_day = 0;
				            			$average_guest = 0;
				    					foreach( $guest_data as $guest ){
				    						$count_items_p_day++;
				    						$total_hours_p_day += $guest::diff_guest_time( $guest->start_time, $guest->end_time, 1 );
				    					}

				    					if ( $total_hours_p_day > 0 ) {
				    						$average_guest = ( $total_hours_p_day / $count_items_p_day );
				    						$average_guest = round( $average_guest, 2 );
				    					}
				            		@endphp
				            		"{{ $average_guest }}",
				            	@endfor
				            ],
				            backgroundColor: ["rgba(255, 99, 132, 0.2)","rgba(255, 159, 64, 0.2)","rgba(255, 205, 86, 0.2)","rgba(75, 192, 192, 0.2)","rgba(54, 162, 235, 0.2)","rgba(153, 102, 255, 0.2)","rgba(201, 203, 207, 0.2)"],"borderColor":["rgb(255, 99, 132)","rgb(255, 159, 64)","rgb(255, 205, 86)","rgb(75, 192, 192)","rgb(54, 162, 235)","rgb(153, 102, 255)","rgb(201, 203, 207)"],
				            borderColor: ["rgba(255, 99, 132, 0.2)","rgba(255, 159, 64, 0.2)","rgba(255, 205, 86, 0.2)","rgba(75, 192, 192, 0.2)","rgba(54, 162, 235, 0.2)","rgba(153, 102, 255, 0.2)","rgba(201, 203, 207, 0.2)"],"borderColor":["rgb(255, 99, 132)","rgb(255, 159, 64)","rgb(255, 205, 86)","rgb(75, 192, 192)","rgb(54, 162, 235)","rgb(153, 102, 255)","rgb(201, 203, 207)"],
				            borderWidth: 1
				        },
				    ],
				    "visits_gender_chart" : [
				    	{
                            label:[ 'Femenino', 'Masculino' ],
                            data: [
                               	@php

									$total_genders_visits = [ 'm' => 0, 'f' => 0 ];
								
								@endphp
                                @for($i = 1; $i <= 7; $i++)
                                    @php

                                        $date = \Carbon\Carbon::now()->subDays(7)->addDays($i)->format('Y-m-d'); 

                                        $guests = App\Models\Zones\Guest::where('start_time', 'like', '%' . $date . '%')
                                            ->where('zone_id', $zone->zone_id )
                                            ->get();

                                        foreach( $guests as $guest_gender ){
                                        	$total_genders_visits[ $guest_gender->gender ] += 1;
                                        }
                                       	
                                    @endphp
                                @endfor
                               	"{{ $total_genders_visits['f'] }}","{{ $total_genders_visits['m'] }}"
                            ],
                            backgroundColor: [ '#F78181', '#5858FA' ],
                            borderColor: [ '#F78181', '#5858FA' ],
                            borderWidth: 1
                        },
				    ],--}}
			        @endrole
				}

				var chart_types = {
					"message_chart" : "bar",
					@role('contact')
					{{-- "visits_time_chart" : "bar", 
					"visits_gender_chart" : "pie",--}}
					@endrole
				}

				var chart_options = {
					"message_chart" : {
					    type: chart_types[ container ],
					    data: {
					        labels: [
					        	@foreach( $categories as $category )
					        	"{{ $category['name'] }}",
					        	@endforeach
					        ],
					        datasets: dataset[ container ]
					    },
					    options: {
					    	legend : false,
					    	scales: {
					    		yAxes: [
					    			{
					    				ticks: {
					    					beginAtZero:true
					    				}
					    			}
					    		]
					    	}
					    }
					},
					@role('contact')
					{{--"visits_time_chart" : {
					    type: chart_types[ container ],
					    data: {
					        labels: [
					        	@for($i = 1; $i <= 7; $i++)
					        	"{{ \Carbon\Carbon::now()->subDays(7)->addDays($i)->format('d M') }}", 
					        	@endfor
					        ],
					        datasets: dataset[ container ]
					    },
					    options: {
					    	legend : false
					    }
					},
					"visits_gender_chart" : {
					    type: chart_types[ container ],
					    data: {
					        labels: [
					        	@for($i = 1; $i <= 7; $i++)
					        	"{{ \Carbon\Carbon::now()->subDays(7)->addDays($i)->format('d M') }}", 
					        	@endfor
					        ],
					        datasets: dataset[ container ]
					    },
					    options: {
					    	legend : false
					    }
					},--}}
					@endrole
				}

				var ctx = document.getElementById(container).getContext('2d');
				var myChart = new Chart(ctx, chart_options[ container ]);
			}


			build_messages_by_category_chart();
			@role('contact')
			{{--build_messages_by_category_chart('visits_time_chart');
			build_messages_by_category_chart('visits_gender_chart');--}}
	        @endrole
		});
	</script>
	

@stop