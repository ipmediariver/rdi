@extends('layouts.zone')

@section('right-content')
<div class="panel panel-default mb-0">
	<div class="panel-heading d-flex align-items-center">
		<h3 class="panel-title">Lista de tareas</h3>
	</div>
	@role('agent')
	@else
	<div class="panel-body">
		<form action="{{ route('store-task', $zone->zone_id) }}/" method="POST" class="d-flex align-items-center">
			{{ csrf_field() }}
			<input type="text" placeholder="Nueva tarea..." name="description" class="form-control mb-xs-1" autofocus>
			<select name="priority" class="form-control mx-10 mx-xs-10 mb-xs-1">
				<option value="h">Alta</option>
				<option value="n" selected>Normal</option>
				<option value="l">Baja</option>
			</select>
			<button class="btn btn-primary btn-xs-block">Agregar Tarea</button>
		</form>
	</div>
	@endrole
	@if(count($tasks) > 0)
	<div class="table-responsive">
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>Tarea</th>
					<th>Prioridad</th>
					<th>Fecha</th>
					@role('agent')
					@else
					<th class="text-center">Eliminar</th>
					@endrole
				</tr>
			</thead>
			<tbody>
				@foreach($tasks as $task)
				<tr>
					<td style="vertical-align: middle;">{{ $task->description }}</td>
					<td style="vertical-align: middle;">
						@php
							$priority = null;
							$class = null;
							switch ($task->priority) {
								case 'l':
									$priority = 'Baja';
									$class = 'default';
								break;
								case 'h':
									$priority = 'Alta';
									$class = 'danger';
								break;
								default:
									$priority = 'Normal';
									$class = 'primary';
								break;
							}
						@endphp
						<span class="label label-{{ $class }}" style="display: block;">{{ $priority }}</span>
					</td>
					<td style="white-space: nowrap; vertical-align: middle;">{{ $task->created_at->format('d M, Y') }}</td>
					@role('agent')
					
					@else

					<td class="text-center" style="white-space: nowrap; vertical-align: middle;">
						<form action="{{ route('delete-task', ['zone' => $zone->zone_id, 'task' => $task->task_id]) }}" method="POST">
							{{ method_field('DELETE') }}
							{{ csrf_field() }}
							<button type="submit" class="btn btn-link" onclick="return confirm('¿Estas seguro que deseas eliminar esta tarea?')">
								Eliminar tarea
							</button>
						</form>
					</td>

					@endrole
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	@else
	<div class="panel-body">
		<p class="m-0">No se han agregado tareas a esta zona</p>
	</div>
	@endif
</div>
{{ $tasks->links() }}
@stop