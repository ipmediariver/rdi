<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
|
| routes are loaded by the RouteServiceProvider within a group which
|
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {

    $user = auth()->user();

    if (!$user) {

        return redirect()->route('login');

    } else {

        return redirect()->route('dashboard');

    }

});

Route::get('/home', function () {

    return redirect()->route('dashboard');

});

Route::get('/home/charts', 'HomeController@get_chart_dashboard_data' )->name('home-charts');

Auth::routes();

Route::group(['middleware' => 'auth'], function(){
    Route::get('register', function(){

        return redirect()->route('login');

    });

    Route::get('/dashboard', 'HomeController@index')->name('dashboard');

    // Profile Routes
    Route::group(['prefix' => 'profile'], function(){

        Route::get('/', 'UsersController@profile')->name('profile');

        Route::post('/update_user', 'UsersController@update_user')->name('update_user');
        
        Route::post('/update_password', 'UsersController@update_password')->name('update_password');

    });

    // Agents Routes
    Route::resource('agents', 'Users\AgentsController', ['middleware' => 'auth', 'names' => [

        'index'   => 'agents',
        'create'  => 'new-agent',
        'store'   => 'store-agent',
        'show'    => 'show-agent',
        'edit'    => 'edit-agent',
        'update'  => 'update-agent',
        'destroy' => 'delete-agent',

    ]]);


    // Contacts Routes
    Route::resource('contacts', 'Users\ContactsController', ['middleware' => ['auth'], 'names' => [

        'index'   => 'contacts',
        'create'  => 'new-contact',
        'store'   => 'store-contact',
        'show'    => 'show-contact',
        'edit'    => 'edit-contact',
        'update'  => 'update-contact',
        'destroy' => 'delete-contact',

    ]]);


    // Admins Routes
    Route::resource('admins', 'UsersController', ['middleware' => 'auth', 'names' => [

        'index'   => 'admins',
        'create'  => 'new-admin',
        'store'   => 'store-admin',
        'show'    => 'show-admin',
        'edit'    => 'edit-admin',
        'update'  => 'update-admin',
        'destroy' => 'delete-admin',

    ]]);


    // Zones Routes
    Route::resource('zones', 'Zones\ZonesController', ['middleware' => 'auth', 'names' => [

        'index'   => 'zones',
        'create'  => 'new-zone',
        'show'    => 'show-zone',
        'store'   => 'store-zone',
        'edit'    => 'edit-zone',
        'update'  => 'update-zone',
        'destroy' => 'delete-zone',

    ]]);

    // Guests Routes (ADVIS)
    Route::resource('zones.guests', 'Zones\GuestsController', ['middleware' => 'auth', 'names' => [

        'index'   => 'guests',
        'create'  => 'new-guest',
        'show'    => 'show-guest',
        'store'   => 'store-guest',
        'edit'    => 'edit-guest',
        'update'  => 'update-guest',
        'destroy' => 'delete-guest',

    ]]);


    Route::patch('zones/{zone}/guests/{guest}/update-end-time', 'Zones\GuestsController@set_end_time_on_each_guest')
        ->name('update-end-time');



    // Zone Agents
    Route::resource('zones.agents', 'Zones\AgentsController', ['middleware' => 'auth', 'names' => [

        'index'   => 'zone-agents',
        'store'   => 'assign-agent-to-zone',
        'destroy' => 'unassign-agent-from-zone',

    ]]);


    // Zone Contacts
    Route::resource('zones.contacts', 'Zones\ContactsController', ['middleware' => 'auth', 'names' => [

        'index'   => 'zone-contacts',
        'store'   => 'assign-contact-to-zone',
        'destroy' => 'unassign-contact-from-zone',

    ]]);


    // Zone Messages
    Route::resource('zones.messages', 'Zones\MessagesController', ['middleware' => 'auth', 'names' => [

        'index'                      => 'messages',
        'create'                     => 'new-message',
        'show'                       => 'show-message',
        'store'                      => 'store-message',
        'edit'                       => 'edit-message',
        'update'                     => 'update-message',
        'destroy'                    => 'delete-message',

    ]]);

    // Zone Departments
    Route::resource('departments', 'Zones\DepartmentController', ['middleware' => 'auth', 'names' => [

        'index'                      => 'departments',
        'create'                     => 'new-department',
        'show'                       => 'show-department',
        'store'                      => 'store-department',
        'edit'                       => 'edit-department',
        'update'                     => 'update-department',
        'destroy'                    => 'delete-department',

    ]]);

    // Zone create messages
    Route::post('messages/{zone}/{message}', 'Zones\MessagesController@create_comment_for_message')->name('create-message');

    // Validate Message
    Route::post('zones/{zone}/messages/{message}/validate', 'Zones\MessagesController@validate_message')
        ->name('validate-message'); 


    // Categories Routes
    Route::resource('categories', 'Zones\CategoriesController', ['middleware' => 'auth', 'names' => [

        'index'   => 'categories',
        'create'  => 'new-category',
        'show'    => 'show-category',
        'store'   => 'store-category',
        'edit'    => 'edit-category',
        'update'  => 'update-category',
        'destroy' => 'delete-category',

    ]]);

    // Zone Tasks
    Route::resource('zones.tasks', 'Zones\TasksController', ['middleware' => 'auth', 'names' => [

        'index'   => 'tasks',
        'store'   => 'store-task',
        'destroy' => 'delete-task',

    ]]);

    // Zone Category Contacts
    Route::resource('zones.contacts.categories','Zones\ZoneCategoryContactController', ['middleware' => 'auth']);

    // Notifications
    Route::get('notifications', 'HomeController@notifications')->name('notifications');
    Route::get('mark_notifications_as_read/{notification}', 'HomeController@mark_notifications_as_read')->name('mark_notifications_as_read');
    Route::get('mark_all_notifications_as_read', 'HomeController@mark_all_notifications_as_read')->name('mark_all_notifications_as_read');

    //Check in
    
    Route::group(['prefix' => 'checkin'], function(){
        
        Route::get('/{zone}/checks', 'CheckInController@index')->name('checks');

        Route::post('/{zone}', 'CheckInController@store')->name('create-checkin');

        Route::post('/{zone}/close/{chekin}', 'CheckInController@update')->name('close-checkin');

        Route::delete('/{zone}/destroy/{checkin}', 'CheckInController@destroy')->name('delete-checkin');

    });


    Route::group(['prefix' => 'reports'], function(){

        Route::get('/', 'ReportController@index')->name('reports');

        Route::post('/generate_report', 'ReportController@generate_report')->name('generate_report');

        Route::get('/activity_by_category', 'ReportController@activity_by_category')->name('activity_by_category_report');

        Route::get('/activity_by_agent', 'ReportController@activity_by_agent')->name('activity_by_agent_report');

        Route::get('/activity_by_schedule', 'ReportController@activity_by_schedule')->name('activity_by_schedule_report');

        Route::get('/total_verified_messages', 'ReportController@total_verified_messages')->name('total_verified_messages_report');

        Route::get('/total_severity_messages', 'ReportController@total_severity_messages')->name('total_severity_messages_report');
        
        Route::post('/dowload_report', 'ReportController@download_report')->name('download_report');

    });


});